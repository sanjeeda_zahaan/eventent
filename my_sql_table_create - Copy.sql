create database if not exists ticketbookingsystem;
use ticketbookingsystem;
drop table if exists `register`, `user_table`, `event_table`, `user_event`, `registration` ;


CREATE TABLE `register` 
(
   `id`         int NOT NULL AUTO_INCREMENT,
   `name`       VARCHAR(64)  DEFAULT NULL,
   `phone`      BIGINT(11)  DEFAULT NULL,
   `email`      VARCHAR(64)  DEFAULT NULL,
   `password`   VARCHAR(64) DEFAULT NULL,
   `activated`  TINYINT(2)  DEFAULT NULL,
   `token`      VARCHAR(100)  DEFAULT NULL,
   `created_on` DATE DEFAULT NULL,
   `profileimg` VARCHAR(255) DEFAULT NULL,
   `country`    VARCHAR(64) DEFAULT NULL,
   `city`       VARCHAR(64) DEFAULT NULL,
   `org_name`   VARCHAR(64) DEFAULT NULL,
   `org_des`    VARCHAR(200) DEFAULT NULL,
    PRIMARY KEY (`id`)

);
CREATE TABLE `user_table` 
(
   `id`         int not null AUTO_INCREMENT,
   `name`       VARCHAR(64)  DEFAULT NULL,
   `phone`      BIGINT(11)  DEFAULT NULL,
   `email`      VARCHAR(64)  DEFAULT NULL,
   `password`   VARCHAR(64) DEFAULT NULL,
   `activated`  TINYINT(2)  DEFAULT NULL,
   `token`      VARCHAR(100)  DEFAULT NULL,
   `created_on` DATE DEFAULT NULL,
   `profileimg` varchar(255) DEFAULT NULL,
   `country`    VARCHAR(64) DEFAULT NULL,
   `city`       VARCHAR(64) DEFAULT NULL,
   `org_name`   VARCHAR(64) DEFAULT NULL,
   `org_des`    VARCHAR(200) DEFAULT NULL,
    PRIMARY KEY (`id`)
);
create table `event_table`
(
    `event_id`        int not null AUTO_INCREMENT,
    `user_id`         int(123) not null,
    `title`           varchar(100) not null,
    `ev_location`     varchar(100) not null,
    `sdate`           date not null,
    `stime`           time not null,
    `edate`           date not null,
    `etime`           time not null,
    `ev_type`         varchar(100) not null,
    `ev_topic`        varchar(50) not null,
    `description`     varchar(600) not null,
    `quantity`        int not null,
    `remaining`       int,
    `sponsored_by`    varchar(100) default null,
    `ticket_type`     varchar(50) not null,
    `privacy`         varchar(50) not null,
    `status`          int default null,
    `cover_image`     varchar(50) default null,
    foreign key (`user_id`)  references `register` (`id`),
    primary key      (`event_id`) 
);
create table `user_event`
(
    `user_ev_id`             int not null AUTO_INCREMENT,
    `user_id`                int not null,
    `event_id`               int not null,
    foreign key (`user_id`)  references `user_table` (`id`),
    foreign key (`event_id`) references `event_table` (`event_id`),
    primary key (user_ev_id)
);
create table  `registration`
(
    `reg_id`                 int not null AUTO_INCREMENT,
    `user_id`                int not null,
    `event_id`               int not null,
    `date_time`              datetime not null,
    foreign key (`user_id`)  references `user_table` (`id`),
    foreign key (`event_id`) references `event_table` (`event_id`),
    primary key (`reg_id`)
)