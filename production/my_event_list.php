<?php require_once('top_navbar.php'); ?>

        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="clearfix"></div>
             <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_content">
                   <h2><small>Event List</small></h2>

                    <div class="table-responsive">
                      <table class="table table-striped jambo_table bulk_action">
                        <thead>
                          <tr class="headings">
                            <th>
                              <input type="checkbox" id="check-all" class="flat">
                            </th>
                            <th class="column-title">Title </th>
                            <th class="column-title">Location </th>
                            <th class="column-title">Starting From </th>
                            <th class="column-title">Ending At</th>

                            <th class="column-title">Ticket Quantity </th>
                            <th class="column-title">Remaining Tickets  </th>
                            <th class="column-title">Edit </th>
                            <th class="column-title no-link last"><span class="nobr">View</span>
                            </th>
                              <th class="column-title no-link last"><span class="nobr">Delete</span>
                              </th>
                            <th class="bulk-actions" colspan="7">
                              <a class="antoo" style="color:#fff; font-weight:500;">Bulk Actions ( <span class="action-cnt"> </span> ) <i class="fa fa-chevron-down"></i></a>
                            </th>
                          </tr>
                        </thead>

                        <tbody>
                        <?php
                        $query = "select * from event_table WHERE user_id = '".$_SESSION['user_id']."' ORDER BY event_id DESC";
                        $result = mysqli_query($connection, $query);
                        $data = mysqli_fetch_all($result, MYSQLI_ASSOC);
                        ?>
                        <?php foreach ($data as $row): ?>
                          <tr class="even pointer">
                            <td class="a-center ">
                              <input type="checkbox" class="flat" name="table_records">
                            </td>
                            <td class=" "><p><?= $row['title'] ?></p></td>
                            <td class=" "><?= $row['ev_location'] ?></td>
                            <td class=" "><?= date("d, F h:i a", strtotime($row['sdate'] . ' ' . $row['stime']))?></td>
                            <td class=" "><?= date("d, F h:i a", strtotime($row['edate'] . ' ' . $row['etime']))?></td>

                            <td class=" "><?= $row['quantity'] ?></td>
                            <td class=" "><?= $row['remaining'] ?></td>
                            <td class=" last"><a href="../INtravel/venue_details.php?id=<?= $row['event_id']?>">View</a>
                            <td class=" last"><a href="edit_event.php?id=<?= $row['event_id']?>">Edit</a>
                            <td class=" last"><a onclick="return confirm('Are you sure you want to delete this event?')" href="delete_event.php?id=<?= $row['event_id']?>">Delete</a>
                            </td>
                          </tr>
                        <?php endforeach; ?>

                        </tbody>
                      </table>
                    </div>


                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->

        <!-- footer content -->
        <footer>
          <div class="pull-right">
            Gentelella - Bootstrap Admin Template by <a href="https://colorlib.com">Colorlib</a>
          </div>
          <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->
      </div>
    </div>

    <!-- jQuery -->
    <script src="../vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="../vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="../vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="../vendors/nprogress/nprogress.js"></script>
    <!-- iCheck -->
    <script src="../vendors/iCheck/icheck.min.js"></script>

    <!-- Custom Theme Scripts -->
    <script src="../build/js/custom.min.js"></script>
  </body>
</html>
