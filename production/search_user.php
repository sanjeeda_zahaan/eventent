<?php
 include 'connection.php';
?>
<?php

 //preventing from entering in the home page without login with correct email and pass
 if($_SESSION['id']!=session_id())
 {
  header("Location: log_in.php");
 }

 ?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

    <title>profile</title>

    <!-- Bootstrap -->
    <link href="../vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="../vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="../vendors/nprogress/nprogress.css" rel="stylesheet">
    <!-- bootstrap-daterangepicker -->
    <link href="../vendors/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">

    <!-- Custom Theme Style -->
    <link href="../build/css/custom.min.css" rel="stylesheet">
  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              <a href="index.html" class="site_title"><i class="fa fa-group"></i> <span>Eventent</span></a>
            </div>

            <div class="clearfix"></div>

            <!-- menu profile quick info -->
            <!-- //////////////////// side menu te name \\\\\\\\\\\\\\\\\\ -->
            <div class="profile clearfix">
              <div class="profile_pic">

<!-- ......................... side menu picture of the user........................................ -->
                <?php
                // echo "ID: " . $_SESSION['email'];
                $table = mysqli_query($connection, 'SELECT * FROM register WHERE phone =' . $_SESSION['phone'])or die("Error: " . mysqli_error($connection));
                // $table = mysqli_query($connection, "SELECT * FROM register WHERE email =" . $_SESSION['email']);
                while($row  = mysqli_fetch_array($table)){ ?>


                        <div class="profile_img">

                        <?php  if ($row['profileimg'] == "") {
                        echo "<img src = 'images/default-avatar.jpg' alt = '...' class='img-circle profile_img'>" . "</br>";
                        } else {
                        echo "<img src = 'images/" . $row['profileimg'] . "' alt = '...' class='img-circle profile_img'>" . "</br>";
                        }
                        ?>
                      </div>


              <?php }
              ?>
<!-- ........................ end of side menu picture ................................................ -->


                <!-- <img src="images/img.jpg" alt="..." class="img-circle profile_img"> -->
              </div>
              <div class="profile_info">
                <span>User:</span>
                <h2> <?php echo $_SESSION['name']; ?></h2>
              </div>
            </div>
            <!-- /menu profile quick info -->
            <br />
<!-- ....................................... start of menu options....................................... -->
            <!-- sidebar menu -->
            <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
              <div class="menu_section">
                <!-- <h3></h3> -->
                <ul class="nav side-menu">
                  <!-- ///////////////////////// home \\\\\\\\\\\\\\\\\\\\\ -->
                  <li><a href="profile.php"><i class="fa fa-home"></i> Home <span class="fa fa-chevron"></span></a>

                  </li>
                  <!-- ///////////////////////// profile \\\\\\\\\\\\\\\\\\\\\ -->
                  <li><a href="profile.php"><i class="fa fa-user"></i> Profile <span class="fa fa-chevron"></span></a>

                  </li>
                  <!-- //////////////////////////////////// inbox\\\\\\\\\\\\\\\\\\\\\\\\\ -->
                  <li><a><i class="fa fa-envelope"></i> Inbox <span class="fa fa-chevron"></span></a>

                  </li>
                  <!-- //////////////////////////////////// inbox\\\\\\\\\\\\\\\\\\\\\\\\\ -->
                  <li><a><i class="fa fa-bell"></i> Notifications <span class="fa fa-chevron"></span></a>

                  </li>
                  <!-- ///////////////////////////// create event \\\\\\\\\\\\\\\\\\\\\\\\\ -->
                  <li><a href="create_event.php"><i class="fa fa-edit"></i> Create Event <span class="fa fa-chevron"></span></a>

                  </li>
                  <!-- /////////////////////////////////// Trending event \\\\\\\\\\\\\\\\\\\\\\\\ -->
                  <li><a><i class="fa fa-volume-up"></i> Trending Events <span></span></a>

                  </li>

                  <!-- /////////////////////////////// my events \\\\\\\\\\\\\\\\\\\\\ -->
                  <li><a><i class="fa fa-desktop"></i> My Events <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="my_event_list.php">Launched</a></li>
                      <li><a href="calendar.html">Attending</a></li>
                      <li><a href="inbox.html">Interested</a></li>
                      <li><a href="calendar.html">Drafts</a></li>
                    </ul>
                  </li>

                  <!-- /////////////////////////////////// privacy and settings \\\\\\\\\\\\\\\\\\\\\\\\ -->
                  <li><a><i class="fa fa-wrench"></i> Privacy & Settings <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="changePass_old.php">Change Password</a></li>
                    </ul>
                  </li>
                  <!-- ///////////////////////////////////// log out \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\ -->
                <li><a href="logout.php"><i class="fa fa-sign-out"></i> Log Out <span></span></a>
              <!-- //////////////////////////////////////////////////////////////////////////// -->
                </ul>
              </div>
            </div>
            <!-- /sidebar menu -->
<!-- ......................end of side menu .................................... -->


  <!-- ...............nav bar............................           -->
</div>
      </div>
      <!-- top navigation -->
      <div class="top_nav">
        <div class="nav_menu">
          <nav>
            <div class="nav toggle">
              <a id="menu_toggle"><i class="fa fa-bars"></i></a>
            </div>

            <ul class="nav navbar-nav navbar-right">
              <li class="">
                <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                <img src="images/tent2.png" alt="">
                 <?php echo $_SESSION['name']; ?>
                </a>
              </li>
            </ul>
          </nav>
        </div>
      </div>

  <!-- .................................................................... -->
        <!-- /top navigation -->

        <!-- page content -->

  <!-- //////////////////////////////////////// search bar \\\\\\\\\\\\\\\\\\\\\\\\\\\\\ -->
        <div class="right_col" role="main">
          <div class="">

            <div class="clearfix"></div>
<!-- ///////////////////////////////// profile row \\\\\\\\\\\\\\\\\\\\\\\\\ -->
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Search Organizer or Organization</h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a href="#">Settings 1</a>
                          </li>
                          <li><a href="#">Settings 2</a>
                          </li>
                        </ul>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
  <!-- ...................................... profile start......................................... -->

   <br />
<div class="col-md-5 col-sm-5 col-xs-12 form-group pull-left top_search">
   <div class="form-group">
    <div class="input-group">
     <span class="input-group-addon">Search</span>
     <input type="text" name="search_text" id="search_text" placeholder="Search organizer or organization name" class="form-control" />
    </div>
  </div>
   </div>

   <br />
   <br>
   <br>
   <br>
   <br>
   <br>
<div class="col-md-5 col-md-5 col-xs-12 form-group pull-left ">
   <div id="result"></div>
</div>

<!-- ............................................................................................... -->

  <div class="panel">
    <div class="panel-heading">

    </div>
  <div class="panel-body">


<!-- .................................... edit page option................................. -->


  <!-- ................................................................................. -->
</div>
</div>
</div>

<div class="container">
<br/>
<br/>
<br/>
<br/>
 <!-- <h2> <strong> Profile </strong></h2> -->
 <!-- <p></p> -->



</div>




</body>



<script>
$(document).ready(function(){

 load_data();

 function load_data(query)
 {
  $.ajax({
   url:"search_action.php",
   method:"POST",
   data:{query:query},
   success:function(data)
   {
    $('#result').html(data);
   }
  });
 }
 $('#search_text').keyup(function(){
  var search = $(this).val();
  if(search != '')
  {
   load_data(search);
  }
  else
  {
   load_data();
  }
 });
});
</script>



<!-- .................................................................. -->
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

        <!-- /page content -->

                <footer>
                  <div class="pull-right">
                    Gentelella - Bootstrap Admin Template by <a href="https://colorlib.com">Colorlib</a>
                  </div>
                  <div class="clearfix"></div>
                </footer>


      </div>
    </div>

    <!-- jQuery -->
    <script src="../vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="../vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="../vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="../vendors/nprogress/nprogress.js"></script>
    <!-- morris.js -->
    <script src="../vendors/raphael/raphael.min.js"></script>
    <script src="../vendors/morris.js/morris.min.js"></script>
    <!-- bootstrap-progressbar -->
    <script src="../vendors/bootstrap-progressbar/bootstrap-progressbar.min.js"></script>
    <!-- bootstrap-daterangepicker -->
    <script src="../vendors/moment/min/moment.min.js"></script>
    <script src="../vendors/bootstrap-daterangepicker/daterangepicker.js"></script>

    <!-- Custom Theme Scripts -->
    <script src="../build/js/custom.min.js"></script>

  </body>
</html>
