<?php


  class ContactUs
  {

      // ...................getter and setter methods................................................
      protected $id;
      protected $email;
      protected $sub;
      protected $message;
      protected $contact_date;
      public $conn;

      function setId($id) { $this->id = $id; }
      function getId() { return $this->id; }
      function setEmail($email) { $this->email = $email; }
      function getEmail() { return $this->email; }
      function setSub($sub) { $this->sub = $sub; }
      function getSub() { return $this->sub; }
      function setMessage($message) { $this->message = $message; }
      function getMessage() { return $this->message; }
      function setContact_date($contact_date) { $this->contact_date = $contact_date; }
      function getContact_date() { return $this->contact_date; }



      // .......................end of getter and setter methods ..........................

      function __construct()
    {
      require 'DbConnect.php';
      $db=new DbConnect();
      $this->conn=$db->connect();
    }
    public function save()
    {
      $sql="INSERT INTO `contact_us`(`id`,`email`,`sub`,`message`,`contact_date`) VALUES (null,:email,:sub,:message,:cdate)";
       $stmt=$this->conn->prepare($sql);
       $stmt->bindParam(':email',$this->email);
          $stmt->bindParam(':sub',$this->sub);
             $stmt->bindParam(':message',$this->message);
                  $stmt->bindParam(':cdate',$this->contact_date);

            try {
              if($stmt->execute()){
                return true;
              }     else {
                return false;
              }

            } catch (Exception $e) {
              echo $e->getMessage();
            }
        }

}


 ?>
