<?php
 include 'connection.php';
?>
<?php

 //preventing from entering in the home page without login with correct email and pass
 if($_SESSION['id']!=session_id())
 {
   header("Location: log_in.php");
 }

 ?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Change Image</title>

    <!-- Bootstrap -->
    <link href="../vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="../vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="../vendors/nprogress/nprogress.css" rel="stylesheet">
    <!-- iCheck -->
    <link href="../vendors/iCheck/skins/flat/green.css" rel="stylesheet">
    <!-- bootstrap-wysiwyg -->
    <link href="../vendors/google-code-prettify/bin/prettify.min.css" rel="stylesheet">
    <!-- Select2 -->
    <link href="../vendors/select2/dist/css/select2.min.css" rel="stylesheet">
    <!-- Switchery -->
    <link href="../vendors/switchery/dist/switchery.min.css" rel="stylesheet">
    <!-- starrr -->
    <link href="../vendors/starrr/dist/starrr.css" rel="stylesheet">
    <!-- bootstrap-daterangepicker -->
    <link href="../vendors/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">

    <!-- Custom Theme Style -->
    <link href="../build/css/custom.min.css" rel="stylesheet">

  </head>
<!-- ..................... body starts (menu and nav bar) ................. -->

<body class="nav-md">
  <div class="container body">
    <div class="main_container">
      <div class="col-md-3 left_col">
        <div class="left_col scroll-view">
          <div class="navbar nav_title" style="border: 0;">
            <a href="index.html" class="site_title"><i class="fa fa-group"></i> <span>Eventent</span></a>
          </div>

          <div class="clearfix"></div>

          <div class="profile clearfix">
            <div class="profile_pic">

<!-- ......................... side menu picture of the user........................................ -->
              <?php
              // echo "ID: " . $_SESSION['email'];
              $table = mysqli_query($connection, 'SELECT * FROM register WHERE phone =' . $_SESSION['phone'])or die("Error: " . mysqli_error($connection));
              // $table = mysqli_query($connection, "SELECT * FROM register WHERE email =" . $_SESSION['email']);
              while($row  = mysqli_fetch_array($table)){ ?>


                      <div class="profile_img">

                      <?php  if ($row['profileimg'] == "") {
                      echo "<img src = 'images/default-avatar.jpg' alt = '...' class='img-circle profile_img'>" . "</br>";
                      } else {
                      echo "<img src = 'images/" . $row['profileimg'] . "' alt = '...' class='img-circle profile_img'>" . "</br>";
                      }
                      ?>
                    </div>


            <?php }
            ?>
<!-- ........................ end of side menu picture ................................................ -->


              <!-- <img src="images/img.jpg" alt="..." class="img-circle profile_img"> -->
            </div>
            <div class="profile_info">
              <span><strong> User: </strong></span>
              <h2> <?php echo $_SESSION['name']; ?></h2>
            </div>
          </div>
          <!-- /menu profile quick info -->
          <br />
          <!-- /menu profile quick info -->


          <!-- sidebar menu -->
          <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
            <div class="menu_section">
              <!-- <h3></h3> -->
              <ul class="nav side-menu">
                <!-- ///////////////////////// home \\\\\\\\\\\\\\\\\\\\\ -->
                <li><a href="logged_in_home.php"><i class="fa fa-home"></i> Home <span class="fa fa-chevron"></span></a>

                </li>
                <!-- ///////////////////////// home \\\\\\\\\\\\\\\\\\\\\ -->
                <li><a href="profile.php"><i class="fa fa-home"></i> Profile <span class="fa fa-chevron"></span></a>

                </li>
                <!-- //////////////////////////////////// inbox\\\\\\\\\\\\\\\\\\\\\\\\\ -->
                <li><a><i class="fa fa-envelope"></i> Inbox <span class="fa fa-chevron"></span></a>

                </li>
                <!-- //////////////////////////////////// inbox\\\\\\\\\\\\\\\\\\\\\\\\\ -->
                <li><a><i class="fa fa-bell"></i> Notifications <span class="fa fa-chevron"></span></a>

                </li>
                <!-- ///////////////////////////// create event \\\\\\\\\\\\\\\\\\\\\\\\\ -->
                <li><a href="create_event.php"><i class="fa fa-edit"></i> Create Event <span class="fa fa-chevron"></span></a>

                </li>
                <!-- /////////////////////////////////// Trending event \\\\\\\\\\\\\\\\\\\\\\\\ -->
                <li><a><i class="fa fa-volume-up"></i> Trending Events <span></span></a>

                </li>
                <!-- /////////////////////////////////// search organization \\\\\\\\\\\\\\\\\\\\\\\\ -->
                <li><a  href="search_user.php"><i class="fa fa-group"></i> Organizations <span></span></a>

                </li>

                <!-- /////////////////////////////// my events \\\\\\\\\\\\\\\\\\\\\ -->
                <li><a><i class="fa fa-desktop"></i> My Events <span class="fa fa-chevron-down"></span></a>
                  <ul class="nav child_menu">
                    <li><a href="my_event_list.php">Launched</a></li>
                    <li><a href="calendar.html">Attending</a></li>
                    <li><a href="inbox.html">Interested</a></li>
                    <li><a href="calendar.html">Drafts</a></li>
                  </ul>
                </li>

                <!-- /////////////////////////////////// privacy and settings \\\\\\\\\\\\\\\\\\\\\\\\ -->
                <li><a><i class="fa fa-wrench"></i> Privacy & Settings <span class="fa fa-chevron-down"></span></a>
                  <ul class="nav child_menu">
                    <li><a href="changePass_old.php">Change Password</a></li>
                  </ul>
                </li>
                <!-- ///////////////////////////////////// log out \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\ -->
              <li><a href="logout.php"><i class="fa fa-sign-out"></i> Log Out <span></span></a>
            <!-- //////////////////////////////////////////////////////////////////////////// -->
              </ul>
            </div>
          </div>
          <!-- /sidebar menu -->
<!-- ......................end of side menu .................................... -->

        </div>
      </div>
      <!-- top navigation -->
  <!-- ..................................... nav bar ................................. -->
  <div class="top_nav">
    <div class="nav_menu">
      <nav>
        <div class="nav toggle">
          <a id="menu_toggle"><i class="fa fa-bars"></i></a>
        </div>

        <ul class="nav navbar-nav navbar-right">
          <li class="">
            <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
            <img src="images/tent2.png" alt="">
             <?php echo $_SESSION['name']; ?>
            </a>
          </li>
        </ul>
      </nav>
    </div>
  </div>

<!-- ............................. end of menu and nav bar .......................... -->

<!-- ///////////////////////////////////////////////// page contents \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\ -->
        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>Search Events</h3>
              </div>

              <div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                  <div class="input-group">
                    <input type="text" class="form-control" placeholder="Search for...">
                    <span class="input-group-btn">
                      <button class="btn btn-default" type="button">Go!</button>
                    </span>
                  </div>
                </div>
              </div>
            </div>
            <!-- ......................................... -->
            <div class="clearfix"></div>
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h1>Change Image</h1> <h2> <small>Upload your image.</small></h2>
  <!-- ............................. form .................................... -->

  <div class="container">
    <div class="row">

   <!-- ///////////////////////////////////////\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\ -->

    <div class="col-md-6 col-md-offset-1 sign-up">
      <div class="panel">
        <div class="panel-heading">
              <!-- <hr>
                <h2 class="intro-text text-center"> <strong> Change Password </strong> </h2>
              <hr> -->
        </div>
      <div class="panel-body">
        <!-- <form method="post" action="clone_pf.php?edit_form = <?php echo $p ?>" enctype="multipart/form-data"> -->
                <form id="update-image-form" method="post" action="action.php" enctype="multipart/form-data">
                  <div class="form-group">
                  <div class="input-group">
                  <label hidden><h4>ID:</h4>
                  <input type = "email" id="email" name="email" class="form-control"  value="<?php echo $_SESSION['email']; ?>">
                  </label>
                  </div>
                  </div>
                          <div>
                              <input type="file" name="image">
                          </div>
                          </br>
                          <?php
                          $table = mysqli_query($connection, 'SELECT * FROM register WHERE phone =' . $_SESSION['phone'])or die("Error: " . mysqli_error($connection));
                          while($row  = mysqli_fetch_array($table)){ ?>
                                <div class="x_content">
                                <div class="col-md-3 col-sm-3 col-xs-12 profile_left">
                                <div class="profile_img">
                                <div id="crop-avatar">
                                </br>
                                <?php  if ($row['profileimg'] == "") {
                                echo "<img width = '150' height = '150'
                                 src = 'images/default-avatar.jpg' alt = 'Default Profile Picture'>" . "</br>";
                                }
                                else
                                {
                                echo "<img width = '150' height = '150'
                                src = 'images/" . $row['profileimg'] . "' alt = 'Profile Picture'>" . "</br>";
                                }
                                ?>

                              <?php }
                              ?>
                            </div>
                            </div>


                              <br>
                             <div>
                            <input type="submit" id="imgSubmit "name="imgSubmit">
                            </div>

                          </div>
                          </div>
                </form>



        <!-- .......................................... -->


      <!-- //////////////////////// .....................................\\\\\\\\\\\\\\\\\\\ -->

      <hr>
          </div>
            </div>
              </div>
                </div>
              </div>
<!-- ............................end of form.................................................... -->
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
        <!-- /page content -->

        <!-- footer content -->
        <footer>
          <div class="pull-right">
            Gentelella - Bootstrap Admin Template by <a href="https://colorlib.com">Colorlib</a>
          </div>
          <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->
      </div>
    </div>

    <!-- jQuery -->
    <script src="../vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="../vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="../vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="../vendors/nprogress/nprogress.js"></script>
    <!-- bootstrap-progressbar -->
    <script src="../vendors/bootstrap-progressbar/bootstrap-progressbar.min.js"></script>
    <!-- iCheck -->
    <script src="../vendors/iCheck/icheck.min.js"></script>
    <!-- bootstrap-daterangepicker -->
    <script src="../vendors/moment/min/moment.min.js"></script>
    <script src="../vendors/bootstrap-daterangepicker/daterangepicker.js"></script>
    <!-- bootstrap-wysiwyg -->
    <script src="../vendors/bootstrap-wysiwyg/js/bootstrap-wysiwyg.min.js"></script>
    <script src="../vendors/jquery.hotkeys/jquery.hotkeys.js"></script>
    <script src="../vendors/google-code-prettify/src/prettify.js"></script>
    <!-- jQuery Tags Input -->
    <script src="../vendors/jquery.tagsinput/src/jquery.tagsinput.js"></script>
    <!-- Switchery -->
    <script src="../vendors/switchery/dist/switchery.min.js"></script>
    <!-- Select2 -->
    <script src="../vendors/select2/dist/js/select2.full.min.js"></script>
    <!-- Parsley -->
    <script src="../vendors/parsleyjs/dist/parsley.min.js"></script>
    <!-- Autosize -->
    <script src="../vendors/autosize/dist/autosize.min.js"></script>
    <!-- jQuery autocomplete -->
    <script src="../vendors/devbridge-autocomplete/dist/jquery.autocomplete.min.js"></script>
    <!-- starrr -->
    <script src="../vendors/starrr/dist/starrr.js"></script>
    <!-- Custom Theme Scripts -->
    <script src="../build/js/custom.min.js"></script>

  </body>
</html>
