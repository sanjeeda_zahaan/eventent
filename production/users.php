<?php


  class Users
  {


      // ...................getter and setter methods................................................
      protected $id;
      protected $name;
      protected $phone;
      protected $email;
      protected $password;
      protected $activated;
      protected $token;
      protected $createdOn;
      protected $profileimg;
      protected $country;
      protected $city;
      protected $orgName;
      protected $orgDes;
      public $conn;


      function setId($id) { $this->id = $id; }
      function getId() { return $this->id; }
      function setName($name) { $this->name = $name; }
      function getName() { return $this->name; }
      function setPhone($phone) { $this->phone = $phone; }
      function getPhone() { return $this->phone; }
      function setEmail($email) { $this->email = $email; }
      function getEmail() { return $this->email; }
      function setPassword($password) { $this->password = $password; }
      function getPassword() { return $this->password; }
      function setActivated($activated) { $this->activated = $activated; }
      function getActivated() { return $this->activated; }
      function setToken($token) { $this->token = $token; }
      function getToken() { return $this->token; }
      function setCreatedOn($createdOn) { $this->createdOn = $createdOn; }
      function getCreatedOn() { return $this->createdOn; }
      function setProfileimg($profileimg) { $this->profileimg = $profileimg; }
      function getProfileimg() { return $this->profileimg; }
      function setCountry($country) { $this->country = $country; }
      function getCountry() { return $this->country; }
      function setCity($city) { $this->city = $city; }
      function getCity() { return $this->city; }
      function setOrgName($orgName) { $this->orgName = $orgName; }
      function getOrgName() { return $this->orgName; }
      function setOrgDes($orgDes) { $this->orgDes = $orgDes; }
      function getOrgDes() { return $this->orgDes; }

      // .......................end of getter and setter methods ..........................

      function __construct()
    {
      require 'DbConnect.php';
      $db=new DbConnect();
      $this->conn=$db->connect();
    }
    public function save()
    {
      $sql="INSERT INTO `register`(`id`,`name`,`phone`,`email`,`password`,`activated`,`token`,`created_on`,`profileimg`,`country`,`city`,`org_name`,`org_des`) VALUES (null,:name,:phone,:email,:password,:activated,:token,:cdate,:profileimg,:country,:city,:org_name,:org_des)";
       $stmt=$this->conn->prepare($sql);
       $stmt->bindParam(':name',$this->name);
          $stmt->bindParam(':phone',$this->phone);
             $stmt->bindParam(':email',$this->email);
                $stmt->bindParam(':password',$this->password);
                   $stmt->bindParam(':activated',$this->activated);
                      $stmt->bindParam(':token',$this->token);
                         $stmt->bindParam(':cdate',$this->createdOn);
                           $stmt->bindParam(':profileimg',$this->profileimg);
                             $stmt->bindParam(':country',$this->country);
                               $stmt->bindParam(':city',$this->city);
                                 $stmt->bindParam(':org_name',$this->orgName);
                                   $stmt->bindParam(':org_des',$this->orgDes);

            try {
              if($stmt->execute()){
                return true;
              }     else {
                return false;
              }

            } catch (Exception $e) {
              echo $e->getMessage();
            }
        }

  ///////////////////////////////////\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

    public function getUserByEmail()
    {
      $stmt=$this->conn->prepare('SELECT * FROM `register` WHERE email = :email');
      $stmt->bindParam(':email',$this->email);
      try {
        if($stmt->execute())
        {
          $user=$stmt->fetchAll(PDO::FETCH_ASSOC);
        }

      } catch (Exception $e) {
        echo $e->getMessage();
      }
      return $user;
      }

//////////////////////////////////\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\


public function getUserById()
{
  $stmt=$this->conn->prepare('SELECT * FROM `register` WHERE id = :id');
  $stmt->bindParam(':id',$this->id);
  try {
    if($stmt->execute())
    {
      $user=$stmt->fetch(PDO::FETCH_ASSOC);
    }

  } catch (Exception $e) {
    echo $e->getMessage();
  }
  return $user;
  }

  ///////////////////////////////////\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

  public function activateUserAccount()
  {
    $stmt=$this->conn->prepare('UPDATE `register` SET activated = 1 WHERE id = :id');
      $stmt->bindParam(':id',$this->id);
      try {
        if($stmt->execute()){
          return true;
        }     else {
          return false;
        }
      } catch (Exception $e) {
        echo $e->getMessage();
      }
  }
////////////////////////// update token for reset password \\\\\\\\\\\\\\\\\\\\\\\

    public function updateToken()
    {
      $stmt=$this->conn->prepare('UPDATE `register` SET token = :token WHERE id = :id');
        $stmt->bindParam(':token',$this->token);
        $stmt->bindParam(':id',$this->id);
        try {
          if($stmt->execute()){
            return true;
          }     else {
            return false;
          }
        } catch (Exception $e) {
          echo $e->getMessage();
        }
    }

///////////////////////////// update password \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
public function updatePass()
{
  $stmt=$this->conn->prepare('UPDATE `register` SET password = :password WHERE id = :id');
    $stmt->bindParam(':password',$this->password);
    $stmt->bindParam(':id',$this->id);
    try {
      if($stmt->execute()){
        return true;
      }     else {
        return false;
      }
    } catch (Exception $e) {
      echo $e->getMessage();
    }
}

///////////////////////////// change password \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
public function ChangePass()
{
  $stmt=$this->conn->prepare('UPDATE `register` SET password = :password WHERE email = :email');
    $stmt->bindParam(':password',$this->password);
    $stmt->bindParam(':email',$this->email);
    try {
      if($stmt->execute()){
        return true;
      }     else {
        return false;
      }
    } catch (Exception $e) {
      echo $e->getMessage();
    }
}
//////////////////////////////// update image \\\\\\\\\\\\\\\\\\\\\\\
public function updateImage()
{
  $stmt=$this->conn->prepare('UPDATE `register` SET profileimg = :profileimg WHERE email = :email');
    $stmt->bindParam(':profileimg',$this->profileimg);
    $stmt->bindParam(':email',$this->email);
    try {
      if($stmt->execute()){
        return true;
      }     else {
        return false;
      }
    } catch (Exception $e) {
      echo $e->getMessage();
    }
}
////////////////////////////////////////\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    }

 ?>
