<?php require_once('top_navbar.php'); ?>

        <!-- page content -->

        <!-- //////////////////////////////////////// search bar \\\\\\\\\\\\\\\\\\\\\\\\\\\\\ -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>Search Event</h3>
              </div>

              <div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                  <div class="input-group">
                    <input type="text" class="form-control" placeholder="Search for...">
                    <span class="input-group-btn">
                      <button class="btn btn-default" type="button">Go!</button>
                    </span>
                  </div>
                </div>
              </div>
            </div>

            <div class="clearfix"></div>
<!-- ///////////////////////////////// profile row \\\\\\\\\\\\\\\\\\\\\\\\\ -->
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Profile</h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a href="#">Settings 1</a>
                          </li>
                          <li><a href="#">Settings 2</a>
                          </li>
                        </ul>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
  <!-- ...................................... profile start..........................................                 -->


<!-- ........................................................... -->

  <div class="panel">
    <div class="panel-heading">

    </div>
  <div class="panel-body">


<!-- .................................... edit page option................................. -->
<?php
// echo "ID: " . $ID;

$table = mysqli_query($connection, 'SELECT * FROM register WHERE id =' . $ID)or die("Error: " . mysqli_error($connection));

while($row  = mysqli_fetch_array($table)){ ?>
                  <div class="x_content">
                    <div class="col-md-3 col-sm-3 col-xs-12 profile_left">
                      <div class="profile_img">
                        <div id="crop-avatar">
                        </br>
                      <?php  if ($row['profileimg'] == "") {
                            echo "<img width = '150' height = '150'
                               src = 'images/default-avatar.jpg' alt = 'Default Profile Picture'>" . "</br>";
                        } else {
                            echo "<img width = '150' height = '150'
                                 src = 'images/" . $row['profileimg'] . "' alt = 'Profile Picture'>" . "</br>";
                        }
                        ?>
                          <!-- Current avatar -->
                          <!-- <img class="img-responsive avatar-view" src="images/picture.jpg" alt="Avatar" title="Change the avatar"> -->
                        </div>
                      </div>
                    </br>
                      <a class="btn btn-info" href="message.php"><i class="fa fa-envelope m-right-xs"></i> Message</a>

                      <!-- /////////////////// name \\\\\\\\\\\\\\ -->
                    <h3>  <?php echo $row['name']; ?> </h3>


<!-- /////////////////////////////// email, mobile,description\\\\\\\\\\\\\\\\\\\\\\\\\\  -->
                      <ul class="list-unstyled user_data">
                        <li><i class="fa fa-map-marker user-profile-icon"></i>
                          <?php echo $row['country']; ?>
                        </li>

                        <li>
                          <i class="fa fa-map-marker user-profile-icon"></i>

                        <?php echo $row['city']; ?>
                        </li>

                        <li class="m-top-xs">
                          <i class="fa fa-university user-profile-ico"></i>
                        <?php echo $row['org_name']; ?>
                        </li>
                        <li class="m-top-xs">
                          <i class="fa fa-pencil user-profile-icon"></i>
                        <?php echo $row['org_des']; ?>
                        </li>
                      </ul>
                      <!-- ////////////////////////////////// edit profile button \\\\\\\\\\\\\\\\\\\\\\\\\ -->
                      <a class="btn btn-success" href="#" data-role="update" data-id="<?php echo $row['id'] ;?>"><i class="fa fa-edit m-right-xs"></i>Add as friend</a>
                      <!-- <a class="btn btn-success"><i class="fa fa-edit m-right-xs"></i>Edit Profile</a> -->
                    <!-- <a class="btn btn-warning" href="changePass_old.php"><i class="fa fa-edit m-right-xs"></i>Change Password</a> -->
                      <br />
                              </li>
                          </div>
                        </div>
      <?php }
      ?>

  <!-- ................................................................................. -->
</div>
</div>
</div>

<div class="container">
<br/>
<br/>
<br/>
<br/>




</div>



</body>




<!-- .................................................................. -->
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

        <!-- /page content -->

                <footer>
                  <div class="pull-right">
                    Gentelella - Bootstrap Admin Template by <a href="https://colorlib.com">Colorlib</a>
                  </div>
                  <div class="clearfix"></div>
                </footer>


      </div>
    </div>

    <!-- jQuery -->
    <script src="../vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="../vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="../vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="../vendors/nprogress/nprogress.js"></script>
    <!-- morris.js -->
    <script src="../vendors/raphael/raphael.min.js"></script>
    <script src="../vendors/morris.js/morris.min.js"></script>
    <!-- bootstrap-progressbar -->
    <script src="../vendors/bootstrap-progressbar/bootstrap-progressbar.min.js"></script>
    <!-- bootstrap-daterangepicker -->
    <script src="../vendors/moment/min/moment.min.js"></script>
    <script src="../vendors/bootstrap-daterangepicker/daterangepicker.js"></script>

    <!-- Custom Theme Scripts -->
    <script src="../build/js/custom.min.js"></script>

  </body>
</html>
