<?php

require_once('connection.php');

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>EVENTENT !</title>

    <!-- Bootstrap -->
    <link href="../vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="../vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="../vendors/nprogress/nprogress.css" rel="stylesheet">
    <!-- iCheck -->
    <link href="../vendors/iCheck/skins/flat/green.css" rel="stylesheet">
    <!-- bootstrap-wysiwyg -->
    <link href="../vendors/google-code-prettify/bin/prettify.min.css" rel="stylesheet">
    <!-- Select2 -->
    <link href="../vendors/select2/dist/css/select2.min.css" rel="stylesheet">
    <!-- Switchery -->
    <link href="../vendors/switchery/dist/switchery.min.css" rel="stylesheet">
    <!-- starrr -->
    <link href="../vendors/starrr/dist/starrr.css" rel="stylesheet">
    <!-- bootstrap-daterangepicker -->
    <link href="../vendors/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">

    <!-- Custom Theme Style -->
    <link href="../build/css/custom.min.css" rel="stylesheet">
</head>

<body class="nav-md">
<div class="container body">
    <div class="main_container">
        <div class="col-md-3 left_col">
            <div class="left_col scroll-view">
                <div class="navbar nav_title" style="border: 0;">
                    <a href="../INtravel/index.php" class="site_title"><i class="fa fa-paw"></i> <span>EVENTENT !</span></a>
                </div>

                <div class="clearfix"></div>

                <!-- menu profile quick info -->
                <!-- //////////////////// side menu te name \\\\\\\\\\\\\\\\\\ -->
                <div class="profile clearfix">
                    <div class="profile_pic">

                        <!-- ......................... side menu picture of the user........................................ -->
                        <?php

                        $table = mysqli_query($connection, 'SELECT * FROM register WHERE phone =' . $_SESSION['phone'])or die("Error: " . mysqli_error($connection));
                        // $table = mysqli_query($connection, "SELECT * FROM register WHERE email =" . $_SESSION['email']);
                        while($row  = mysqli_fetch_array($table)){ ?>


                            <div class="profile_img">

                                <?php  if ($row['profileimg'] == "") {
                                    echo "<img src = 'images/default-avatar.jpg' alt = '...' class='img-circle profile_img'>" . "</br>";
                                } else {
                                    echo "<img src = 'images/" . $row['profileimg'] . "' alt = '...' class='img-circle profile_img'>" . "</br>";
                                }
                                ?>
                            </div>


                        <?php }
                        ?>
                        <!-- ........................ end of side menu picture ................................................ -->


                        <!-- <img src="images/img.jpg" alt="..." class="img-circle profile_img"> -->
                    </div>
                    <div class="profile_info">
                        <span>Welcome,</span>
                        <h2> <?php echo $_SESSION['name']; ?></h2>
                    </div>
                </div>
                <!-- /menu profile quick info -->
                <br />
                <!-- ....................................... start of menu options....................................... -->
                <!-- sidebar menu -->
                <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
                    <div class="menu_section">
                        <!-- <h3></h3> -->
                        <ul class="nav side-menu">
                            <!-- ///////////////////////// home \\\\\\\\\\\\\\\\\\\\\ -->
                            <li><a href="profile.php"><i class="fa fa-home"></i> Home <span class="fa fa-chevron"></span></a>

                            </li>
                            <!-- //////////////////////////////////// inbox\\\\\\\\\\\\\\\\\\\\\\\\\ -->
                            <li><a><i class="fa fa-envelope"></i> Inbox <span class="fa fa-chevron"></span></a>

                            </li>
                            <!-- //////////////////////////////////// inbox\\\\\\\\\\\\\\\\\\\\\\\\\ -->
                            <li><a><i class="fa fa-bell"></i> Notifications <span class="fa fa-chevron"></span></a>

                            </li>
                            <!-- ///////////////////////////// create event \\\\\\\\\\\\\\\\\\\\\\\\\ -->
                            <li><a href="create_event.php"><i class="fa fa-edit"></i> Create Event <span class="fa fa-chevron"></span></a>

                            </li>

                            <!-- ///////////////////////////// create venue \\\\\\\\\\\\\\\\\\\\\\\\\ -->
                            <li><a href="create_venue.php"><i class="fa fa-edit"></i> Create Venue <span class="fa fa-chevron"></span></a> </li>


                            <!-- /////////////////////////////////// Trending event \\\\\\\\\\\\\\\\\\\\\\\\ -->
                            <li><a href="../INtravel/browse_events.php"><i class="fa fa-volume-up"></i> Trending Events <span></span></a>

                            </li>
                            <!-- /////////////////////////////////// search organization \\\\\\\\\\\\\\\\\\\\\\\\ -->
                            <li><a  href="search_user.php"><i class="fa fa-group"></i> Organizations <span></span></a>

                            </li>

                            <!-- /////////////////////////////// my events \\\\\\\\\\\\\\\\\\\\\ -->
                            <li><a><i class="fa fa-desktop"></i> My Events <span class="fa fa-chevron-down"></span></a>
                                <ul class="nav child_menu">
                                    <li><a href="my_event_list.php">Launched</a></li>
                                    <li><a href="following_events.php">Attending</a></li>
                                    <!--                                    <li><a href="inbox.html">Interested</a></li>-->
                                    <!--                                    <li><a href="calendar.html">Drafts</a></li>-->
                                </ul>
                            </li>

                            <!-- /////////////////////////////////// privacy and settings \\\\\\\\\\\\\\\\\\\\\\\\ -->
                            <li><a><i class="fa fa-wrench"></i> Privacy & Settings <span class="fa fa-chevron-down"></span></a>
                                <ul class="nav child_menu">
                                    <li><a href="changePass_old.php">Change Password</a></li>
                                </ul>
                            </li>
                            <!-- ///////////////////////////////////// log out \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\ -->
                            <li><a href="logout.php"><i class="fa fa-sign-out"></i> Log Out <span></span></a>
                                <!-- //////////////////////////////////////////////////////////////////////////// -->
                        </ul>
                    </div>
                </div>

                <!-- ......................end of side menu .................................... -->


                <!-- ...............nav bar............................ -->
            </div>
        </div>
        <!-- top navigation -->
        <div class="top_nav">
            <div class="nav_menu">
                <nav>
                    <div class="nav toggle">
                        <a id="menu_toggle"><i class="fa fa-bars"></i></a>
                    </div>

                    <ul class="nav navbar-nav navbar-right">
                        <li class="">
                            <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                <img src="images/tent2.png" alt="">
                                <?php echo $_SESSION['name']; ?>
                            </a>
                        </li>
                    </ul>
                </nav>
            </div>
        </div>


        <!-- /top navigation -->