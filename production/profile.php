<?php require_once('../includes/_connection.php');
require_once ('../includes/_session.php');

?>

<?php

 //preventing from entering in the home page without login with correct email and pass
 if($_SESSION['id']!=session_id())
 {
  header("Location: log_in.php");
 }

 ?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

    <title>profile</title>

    <!-- Bootstrap -->
    <link href="../vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="../vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="../vendors/nprogress/nprogress.css" rel="stylesheet">
    <!-- bootstrap-daterangepicker -->
    <link href="../vendors/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">

    <!-- Custom Theme Style -->
    <link href="../build/css/custom.min.css" rel="stylesheet">
  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              <a href="../INtravel/index.php" class="site_title"><i class="fa fa-group"></i> <span>Eventent</span></a>
            </div>

            <div class="clearfix"></div>

            <!-- menu profile quick info -->
            <!-- //////////////////// side menu te name \\\\\\\\\\\\\\\\\\ -->
            <div class="profile clearfix">
              <div class="profile_pic">

<!-- ......................... side menu picture of the user........................................ -->
                <?php
                // echo "ID: " . $_SESSION['email'];
                $table = mysqli_query($connection, 'SELECT * FROM register WHERE phone =' . $_SESSION['phone'])or die("Error: " . mysqli_error($connection));
                // $table = mysqli_query($connection, "SELECT * FROM register WHERE email =" . $_SESSION['email']);
                while($row  = mysqli_fetch_array($table)){ ?>


                        <div class="profile_img">

                        <?php  if ($row['profileimg'] == "") {
                        echo "<img src = 'images/default-avatar.jpg' alt = '...' class='img-circle profile_img'>" . "</br>";
                        } else {
                        echo "<img src = 'images/" . $row['profileimg'] . "' alt = '...' class='img-circle profile_img'>" . "</br>";
                        }
                        ?>
                      </div>


              <?php }
              ?>
<!-- ........................ end of side menu picture ................................................ -->


                <!-- <img src="images/img.jpg" alt="..." class="img-circle profile_img"> -->
              </div>
              <div class="profile_info">
                <span>Welcome,</span>
                <h2> <?php echo $_SESSION['name']; ?></h2>
              </div>
            </div>
            <!-- /menu profile quick info -->
            <br />
<!-- ....................................... start of menu options....................................... -->
            <!-- sidebar menu -->
            <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
              <div class="menu_section">
                <!-- <h3></h3> -->
                <ul class="nav side-menu">
                  <!-- ///////////////////////// home \\\\\\\\\\\\\\\\\\\\\ -->
                  <li><a href="profile.php"><i class="fa fa-home"></i> Home <span class="fa fa-chevron"></span></a>

                  </li>
                  <!-- //////////////////////////////////// inbox\\\\\\\\\\\\\\\\\\\\\\\\\ -->
                  <li><a><i class="fa fa-envelope"></i> Inbox <span class="fa fa-chevron"></span></a>

                  </li>
                  <!-- //////////////////////////////////// inbox\\\\\\\\\\\\\\\\\\\\\\\\\ -->
                  <li><a><i class="fa fa-bell"></i> Notifications <span class="fa fa-chevron"></span></a>

                  </li>
                  <!-- ///////////////////////////// create event \\\\\\\\\\\\\\\\\\\\\\\\\ -->
                  <li><a href="create_event.php"><i class="fa fa-edit"></i> Create Event <span class="fa fa-chevron"></span></a>

                  </li>

                  <!-- ///////////////////////////// create venue \\\\\\\\\\\\\\\\\\\\\\\\\ -->
                  <li><a href="create_venue.php"><i class="fa fa-edit"></i> Create Venue <span class="fa fa-chevron"></span></a> </li>
                  <!-- /////////////////////////////////// Trending event \\\\\\\\\\\\\\\\\\\\\\\\ -->
                  <li><a><i class="fa fa-volume-up"></i> Trending Events <span></span></a>

                  </li>
                  <!-- /////////////////////////////////// search organization \\\\\\\\\\\\\\\\\\\\\\\\ -->
                  <li><a  href="search_user.php"><i class="fa fa-group"></i> Organizations <span></span></a>

                  </li>
                  <!-- /////////////////////////////// my events \\\\\\\\\\\\\\\\\\\\\ -->
                  <li><a><i class="fa fa-desktop"></i> My Events <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="my_event_list.php">Launched</a></li>
                      <li><a href="following_events.php">Attending</a></li>
                      <li><a href="inbox.html">Interested</a></li>
                      <li><a href="calendar.html">Drafts</a></li>
                    </ul>
                  </li>

                  <!-- /////////////////////////////////// privacy and settings \\\\\\\\\\\\\\\\\\\\\\\\ -->
                  <li><a><i class="fa fa-wrench"></i> Privacy & Settings <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="changePass_old.php">Change Password</a></li>
                    </ul>
                  </li>
                  <!-- ///////////////////////////////////// log out \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\ -->
                <li><a href="logout.php"><i class="fa fa-sign-out"></i> Log Out <span></span></a>
              <!-- //////////////////////////////////////////////////////////////////////////// -->
                </ul>
              </div>
            </div>
            <!-- /sidebar menu -->
<!-- ......................end of side menu .................................... -->


  <!-- ...............nav bar............................           -->
</div>
      </div>
      <!-- top navigation -->
      <div class="top_nav">
        <div class="nav_menu">
          <nav>
            <div class="nav toggle">
              <a id="menu_toggle"><i class="fa fa-bars"></i></a>
            </div>

            <ul class="nav navbar-nav navbar-right">
              <li class="">
                <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                <img src="images/tent2.png" alt="">
                 <?php echo $_SESSION['name']; ?>
                </a>
              </li>
            </ul>
          </nav>
        </div>
      </div>

  <!-- .................................................................... -->
        <!-- /top navigation -->

        <!-- page content -->

        <!-- //////////////////////////////////////// search bar \\\\\\\\\\\\\\\\\\\\\\\\\\\\\ -->
        <div class="right_col" role="main">
          <div class="">
<!--            <div class="page-title">-->
<!--              <div class="title_left">-->
<!--                <h3>Search Event</h3>-->
<!--              </div>-->
<!---->
<!--              <div class="title_right">-->
<!--                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">-->
<!--                  <div class="input-group">-->
<!--                    <input type="text" class="form-control" placeholder="Search for...">-->
<!--                    <span class="input-group-btn">-->
<!--                      <button class="btn btn-default" type="button">Go!</button>-->
<!--                    </span>-->
<!--                  </div>-->
<!--                </div>-->
<!--              </div>-->
<!--            </div>-->

            <div class="clearfix"></div>
<!-- ///////////////////////////////// profile row \\\\\\\\\\\\\\\\\\\\\\\\\ -->
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Profile</h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a href="#">Settings 1</a>
                          </li>
                          <li><a href="#">Settings 2</a>
                          </li>
                        </ul>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
  <!-- ...................................... profile start..........................................                 -->


<!-- ........................................................... -->

  <div class="panel">
    <div class="panel-heading">

    </div>
  <div class="panel-body">


<!-- .................................... edit page option................................. -->
<?php
// echo "ID: " . $_SESSION['email'];
$table = mysqli_query($connection, 'SELECT * FROM register WHERE phone =' . $_SESSION['phone'])or die("Error: " . mysqli_error($connection));
// $table = mysqli_query($connection, "SELECT * FROM register WHERE email =" . $_SESSION['email']);
while($row  = mysqli_fetch_array($table)){ ?>
                  <div class="x_content">
                    <div class="col-md-3 col-sm-3 col-xs-12 profile_left">
                      <div class="profile_img">
                        <div id="crop-avatar">
                        </br>
                      <?php  if ($row['profileimg'] == "") {
                            echo "<img width = '150' height = '150'
                               src = 'images/default-avatar.jpg' alt = 'Default Profile Picture'>" . "</br>";
                        } else {
                            echo "<img width = '150' height = '150'
                                 src = 'images/" . $row['profileimg'] . "' alt = 'Profile Picture'>" . "</br>";
                        }
                        ?>
                          <!-- Current avatar -->
                          <!-- <img class="img-responsive avatar-view" src="images/picture.jpg" alt="Avatar" title="Change the avatar"> -->
                        </div>
                      </div>
                    </br>
                      <a class="btn btn-info" href="changeImage.php"><i class="fa fa-edit m-right-xs"></i>Change Image</a>

                      <!-- /////////////////// name \\\\\\\\\\\\\\ -->
                    <h3>  <?php echo $row['name']; ?> </h3>


<!-- /////////////////////////////// email, mobile,description\\\\\\\\\\\\\\\\\\\\\\\\\\  -->
                      <ul class="list-unstyled user_data">
                        <li><i class="fa fa-phone user-profile-icon"></i>
                          <?php echo $row['phone']; ?>
                        </li>

                        <li>
                          <i class="fa fa-envelope user-profile-icon"></i>

                        <?php echo $row['email']; ?>
                        </li>

                        <li class="m-top-xs">
                          <i class="fa fa-map-marker user-profile-icon"></i>
                        <?php echo $row['country']; ?>
                        </li>
                      </ul>
                      <!-- ////////////////////////////////// edit profile button \\\\\\\\\\\\\\\\\\\\\\\\\ -->
                      <a class="btn btn-success" href="#" data-role="update" data-id="<?php echo $row['id'] ;?>"><i class="fa fa-edit m-right-xs"></i>Edit Profile</a>
                      <!-- <a class="btn btn-success"><i class="fa fa-edit m-right-xs"></i>Edit Profile</a> -->
                    <!-- <a class="btn btn-warning" href="changePass_old.php"><i class="fa fa-edit m-right-xs"></i>Change Password</a> -->
                      <br />
                              </li>
                          </div>
                        </div>
      <?php }
      ?>
    <!-- //////////////////////////////////// change password \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\ -->


         <!-- <div class="container-fluid">
        <div class="row">
            <div class="col-md-11 col-md-offset-1">
        <div><a href="update_password.php"><h4>Change Password</h4> </a></div>
        </div>
        </div>
        </div> -->
  <!-- ................................................................................. -->
</div>
</div>
</div>

<div class="container">
<br/>
<br/>
<br/>
<br/>
 <!-- <h2> <strong> Profile </strong></h2> -->
 <!-- <p></p> -->
 <table class="table">
   <thead>
     <tr>
       <th hidden>Name</th>
       <th hidden>Phone Number</th>
       <th hidden>Email Address</th>
       <th hidden>Country</th>
       <th hidden>City</th>
       <th hidden>Organization Name</th>
       <th hidden>Organization Description</th>
       <th hidden>Action</th>
     </tr>
   </thead>
   <tbody>
     <?php
         $table = mysqli_query($connection, 'SELECT * FROM register WHERE phone =' . $_SESSION['phone'])or die("Error: " . mysqli_error($connection));
         // $table = mysqli_query($connection ,'SELECT * FROM register');
         while($row  = mysqli_fetch_array($table)){ ?>
             <tr id="<?php echo $row['id']; ?>">
               <td hidden data-target="name"><?php echo $row['name']; ?></td>
               <td hidden data-target="phone"><?php echo $row['phone']; ?></td>
               <td hidden data-target="email"><?php echo $row['email']; ?></td>
               <td hidden data-target="country"><?php echo $row['country']; ?></td>
               <td hidden data-target="city"><?php echo $row['city']; ?></td>
               <td hidden data-target="org_name"><?php echo $row['org_name']; ?></td>
               <td hidden data-target="org_des"><?php echo $row['org_des']; ?></td>
               <td hidden><a href="#" data-role="update" data-id="<?php echo $row['id'] ;?>">Update</a></td>
             </tr>
        <?php }
      ?>

   </tbody>
 </table>


</div>

   <!-- Modal -->
   <div id="myModal" class="modal fade" role="dialog">
     <div class="modal-dialog">

       <!-- Modal content-->
       <div class="modal-content">
         <div class="modal-header">
           <button type="button" class="close" data-dismiss="modal">&times;</button>
           <h4 class="modal-title"><strong>Update Your Information</strong></h4>
         </div>
         <div class="modal-body">
             <div class="form-group">
               <label>Name</label>
               <input type="text" id="name" class="form-control">
             </div>
             <div class="form-group">
               <label>Phone Number</label>
               <input disabled type="text" id="phone" class="form-control">
             </div>

              <div class="form-group">
               <label >Email</label>
               <input disabled type="text" id="email" class="form-control">
             </div>

             <div class="form-group">
              <label>Country</label>
              <input type="text" id="country" class="form-control">
            </div>

            <div class="form-group">
             <label>City</label>
             <input type="text" id="city" class="form-control">
           </div>

             <div class="form-group">
              <label>Organization Name</label>
              <input type="text" id="org_name" class="form-control">
            </div>

            <div class="form-group">
             <label>Organization Description</label>
             <input type="text" id="org_des" class="form-control">
           </div>
               <input type="hidden" id="userId" class="form-control">


         </div>
         <div class="modal-footer">
           <a href="#" id="save" class="btn btn-primary pull-right">Save Changes</a>
           <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
         </div>
       </div>

     </div>
   </div>

</body>

<script>
 $(document).ready(function(){

   //  append values in input fields
     $(document).on('click','a[data-role=update]',function(){
           var id  = $(this).data('id');
           var name  = $('#'+id).children('td[data-target=name]').text();
           var phone  = $('#'+id).children('td[data-target=phone]').text();
           var email  = $('#'+id).children('td[data-target=email]').text();
           var country  = $('#'+id).children('td[data-target=country]').text();
           var city  = $('#'+id).children('td[data-target=city]').text();
           var org_name  = $('#'+id).children('td[data-target=org_name]').text();
           var org_des  = $('#'+id).children('td[data-target=org_des]').text();

           $('#name').val(name);
           $('#phone').val(phone);
           $('#email').val(email);
           $('#country').val(country);
           $('#city').val(city);
           $('#org_name').val(org_name);
           $('#org_des').val(org_des);
           $('#userId').val(id);
           $('#myModal').modal('toggle');
     });

     // now create event to get data from fields and update in database

      $('#save').click(function(){
         var id  = $('#userId').val();
        var name =  $('#name').val();
         var phone =  $('#phone').val();
         var email =   $('#email').val();
         var country =  $('#country').val();
         var city =   $('#city').val();
         var org_name =   $('#org_name').val();
         var org_des =   $('#org_des').val();

         $.ajax({
             url      : 'connection.php',
             method   : 'post',
             data     : {name : name , phone: phone , email : email ,country : country ,city : city,org_name : org_name ,org_des : org_des , id: id},
             success  : function(response){
                           // now update user record in table
                            $('#'+id).children('td[data-target=name]').text(name);
                            $('#'+id).children('td[data-target=phone]').text(phone);
                            $('#'+id).children('td[data-target=email]').text(email);
                            $('#'+id).children('td[data-target=country]').text(country);
                            $('#'+id).children('td[data-target=city]').text(city);
                            $('#'+id).children('td[data-target=org_name]').text(org_name);
                            $('#'+id).children('td[data-target=org_des]').text(org_des);
                            $('#myModal').modal('toggle');

                        }
         });
      });
 });
</script>




<!-- .................................................................. -->
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

        <!-- /page content -->

                <footer>
                  <div class="pull-right">
                    Gentelella - Bootstrap Admin Template by <a href="https://colorlib.com">Colorlib</a>
                  </div>
                  <div class="clearfix"></div>
                </footer>


      </div>
    </div>

    <!-- jQuery -->
    <script src="../vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="../vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="../vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="../vendors/nprogress/nprogress.js"></script>
    <!-- morris.js -->
    <script src="../vendors/raphael/raphael.min.js"></script>
    <script src="../vendors/morris.js/morris.min.js"></script>
    <!-- bootstrap-progressbar -->
    <script src="../vendors/bootstrap-progressbar/bootstrap-progressbar.min.js"></script>
    <!-- bootstrap-daterangepicker -->
    <script src="../vendors/moment/min/moment.min.js"></script>
    <script src="../vendors/bootstrap-daterangepicker/daterangepicker.js"></script>

    <!-- Custom Theme Scripts -->
    <script src="../build/js/custom.min.js"></script>

  </body>
</html>
