<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title> Log In </title>

    <!-- Bootstrap -->
    <link href="../vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="../vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="../vendors/nprogress/nprogress.css" rel="stylesheet">
    <!-- iCheck -->
    <link href="../vendors/iCheck/skins/flat/green.css" rel="stylesheet">
    <!-- bootstrap-wysiwyg -->
    <link href="../vendors/google-code-prettify/bin/prettify.min.css" rel="stylesheet">
    <!-- Select2 -->
    <link href="../vendors/select2/dist/css/select2.min.css" rel="stylesheet">
    <!-- Switchery -->
    <link href="../vendors/switchery/dist/switchery.min.css" rel="stylesheet">
    <!-- starrr -->
    <link href="../vendors/starrr/dist/starrr.css" rel="stylesheet">
    <!-- bootstrap-daterangepicker -->
    <link href="../vendors/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">

    <!-- Custom Theme Style -->
    <link href="../build/css/custom.min.css" rel="stylesheet">

              <style type="text/css">

              .alert  {

                display: none;
              }

              </style>

              <!-- //////////////////////////////// AJAX \\\\\\\\\\\\\\\\\\\\\\\\ -->
           <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
             <script type="text/javascript" >

               $(document).ready(function(){

         ////////////////////////// cookie population \\\\\\\\\\\\\\\\\\\\\\\\
                 $.ajax({
                   url:'action.php',
                   method:'post',
                   data:'action=checkCookie'
                 }).done(function(result){
                  console.log('before result');
                  console.log(result);
                  console.log(' after result');
                  //var data=$.parseJSON(result);
                  // var data=jQuery.parseJSON(result);
                   var data = JSON.parse(result);
                   console.log(' after parse');
                   $('#mail').val(data.email);
                   $('#pwd').val(data.password);
                 });


              ////////////////////////////// email validation for log in \\\\\\\\\\\\\\\\\\\\\\

               $("#mail").keyup(function(){
                 var regexp=/^[a-zA-Z0-9._]+@[a-zA-Z0-9._]+\.[a-zA-Z]{2,4}$/;
                 if(regexp.test($('#mail').val())){
                   $('#mail').closest('.form-group').removeClass('has-error');
                    $('#mail').closest('.form-group').addClass('has-success');
                 } else {
                       $('#mail').closest('.form-group').addClass('has-error');
                 }
               })

             ////////////////// password validation \\\\\\\\\\\\\\\\\\\\\\

               $("#pwd").keyup(function(){
                 var regexp=/^[a-zA-Z0-9]{6,50}$/;
                 if(regexp.test($('#pwd').val())){
                   $('#pwd').closest('.form-group').removeClass('has-error');
                    $('#pwd').closest('.form-group').addClass('has-success');
                 } else {
                       $('#pwd').closest('.form-group').addClass('has-error');
                 }
               })


               ////////////////////////// submit button \\\\\\\\\\\\\\\\\\\\

               $("#submit").click(function(event){
                 event.preventDefault();
                 var formData=$('#sign-in-form').serialize();
                 console.log(formData);
                 $.ajax({
                   url:'action.php',
                   method:'post',
                   data:formData+'&action=submit'
                 }).done(function(result){
                   //console.log('submit result');
                   console.log(result);
                   var data = JSON.parse(result);
                     $('.alert').show();
                   if (data.status == 0) {
                     $('#result').html(data.msg);
                   }else {
                     $('.alert').hide();
                     document.location = "profile.php";
                   }

                       });
                      })
                    })

         </script>


  </head>

  <!-- ..................... body starts (menu and nav bar) ................. -->

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              <a href="../INtravel/index.php" class="site_title"><i class="fa fa-group"></i> <span>Eventent</span></a>
            </div>

            <div class="clearfix"></div>

            <!-- menu profile quick info -->
            <!-- //////////////////// side menu te name \\\\\\\\\\\\\\\\\\ -->
            <div class="profile clearfix">

              <div class="profile_info">
                <!-- <span>Welcome!</span> -->
              </div>
            </div>
            <!-- /menu profile quick info -->
            <br />

            <!-- sidebar menu -->
            <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
              <div class="menu_section">
                <!-- <h3></h3> -->
                <ul class="nav side-menu">
                  <!-- ///////////////////////// home \\\\\\\\\\\\\\\\\\\\\ -->
                  <li><a href="../INtravel/index.php"><i class="fa fa-home"></i> Home <span class="fa fa-chevron"></span></a>

                  </li>

                  <!-- ///////////////////////////// create event \\\\\\\\\\\\\\\\\\\\\\\\\ -->
                  <li><a href="create_event.php"><i class="fa fa-edit"></i> Create Event <span class="fa fa-chevron"></span></a>

                  </li>

                  <!-- /////////////////////////////////// Trending event \\\\\\\\\\\\\\\\\\\\\\\\ -->
                  <li><a><i class="fa fa-volume-up"></i> Trending Events</a>

                  </li>
              <!-- //////////////////////////////////////////////////////////////////////////// -->
                </ul>
              </div>
            </div>
            <!-- /sidebar menu -->
  <!-- ......................end of side menu .................................... -->

          </div>
        </div>
        <!-- top navigation -->
        <!-- ..................................... nav bar ................................. -->
        <div class="top_nav">
          <div class="nav_menu">
            <nav>
              <div class="nav toggle">
                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
              </div>

              <ul class="nav navbar-nav navbar-right">
                <li class="">
                  <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                    <img src="images/default-avatar.jpg" alt=""> Guest
                    <!-- <span class=" fa fa-angle-down"></span> -->
                  </a>
                </li>
              </ul>
            </nav>
          </div>
        </div>
  <!-- ............................. end of menu and nav bar .......................... -->

  <!-- ///////////////////////////////////////////////// page contents \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\ -->


        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>Search events</h3>
              </div>

              <div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                  <div class="input-group">
                    <input type="text" class="form-control" placeholder="Search for...">
                    <span class="input-group-btn">
                      <button class="btn btn-default" type="button">Go!</button>
                    </span>
                  </div>
                </div>
              </div>
            </div>
            <!-- ......................................... -->
            <div class="clearfix"></div>
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h1>Log In</h1><h2><small>Enter your email and password.</small></h2>
                    <br>
                    <hr>
                    <br>
                    <br>
                    <br>
            <!-- ................................................................. -->
            <div class="container">
        <div class="row">
           <div class="col-md-6 col-md-offset-3">
            <div class="alert alert-info alert-dismissible" role="alert">
              <button type="button" class="close" data-dismissible="alert" aria-label="Close">x</button>
              <div id="result"></div>
            </div>
          </div>

        <div class="col-md-6 col-md-offset-3 sign-up">
          <div class="panel">
            <div class="panel-heading">
                  <!-- <hr>
                    <h2 class="intro-text text-center"> <strong> Log In  </strong> </h2>
                  <hr> -->
            </div>



            <div class="row">
              <div class="col-md-9 col-md-offset-9">
            <div>
                  <div class="col-md-9"><a href="resetPass_email.php">Forgot password?</a></div>
            </div>
          </div>
        </div>

          <div class="panel-body">
          <form id="sign-in-form" role="form" method="post" action="" class="form-horizontal">


      <!-- ///////////////// email \\\\\\\\\\\\\\\ -->
          <div class="form-group">
          <div class="input-group">
          <div class="input-group-addon addon-diff-color">
          <span class="glyphicon glyphicon-envelope"></span>
          </div>
          <input type="email" class="form-control" id="mail" name="mail" placeholder="Enter your email">
          </div>
          </div>

      <!-- ////////////// password \\\\\\\\\\\ -->

          <div class="form-group">
          <div class="input-group">
          <div class="input-group-addon addon-diff-color">
          <span class="glyphicon glyphicon-lock"></span>
          </div>
          <input type="password" class="form-control" id="pwd" name="pwd" placeholder="Enter your password">
          </div>
          </div>


      <!-- ////////////////////// submit button \\\\\\\\\\\\\\\\     -->

          <div class="form-group">
            <input type="submit" class="btn btn-success btn-block" value="SUBMIT" name="submit" id="submit">
          </div>

      <!-- ////////////////////////////// remember me \\\\\\\\\\\\\\\\\\\\\\\\\ -->


        <div class="checkbox">
              <label><input type="checkbox" name="remember" id="remember">Remember me</label>
        </div>


          <!-- //////////////////////// .....................................\\\\\\\\\\\\\\\\\\\ -->

          <hr>
          <h4 class=" text-center text-white d-none d-lg-block">
            <span class="text-primary mb-"><strong>Don't have an account?</strong> <a href="sign_up.php">Sign up</a> <strong>here.</strong></span>
          </h4>

            </form>
              </div>
                </div>
                  </div>
                    </div>
                  </div>

<!-- ................................................................................ -->
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
        <!-- /page content -->

        <!-- footer content -->
        <footer>
          <div class="pull-right">
            Gentelella - Bootstrap Admin Template by <a href="https://colorlib.com">Colorlib</a>
          </div>
          <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->
      </div>
    </div>

    <!-- jQuery -->
    <script src="../vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="../vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="../vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="../vendors/nprogress/nprogress.js"></script>
    <!-- bootstrap-progressbar -->
    <script src="../vendors/bootstrap-progressbar/bootstrap-progressbar.min.js"></script>
    <!-- iCheck -->
    <script src="../vendors/iCheck/icheck.min.js"></script>
    <!-- bootstrap-daterangepicker -->
    <script src="../vendors/moment/min/moment.min.js"></script>
    <script src="../vendors/bootstrap-daterangepicker/daterangepicker.js"></script>
    <!-- bootstrap-wysiwyg -->
    <script src="../vendors/bootstrap-wysiwyg/js/bootstrap-wysiwyg.min.js"></script>
    <script src="../vendors/jquery.hotkeys/jquery.hotkeys.js"></script>
    <script src="../vendors/google-code-prettify/src/prettify.js"></script>
    <!-- jQuery Tags Input -->
    <script src="../vendors/jquery.tagsinput/src/jquery.tagsinput.js"></script>
    <!-- Switchery -->
    <script src="../vendors/switchery/dist/switchery.min.js"></script>
    <!-- Select2 -->
    <script src="../vendors/select2/dist/js/select2.full.min.js"></script>
    <!-- Parsley -->
    <script src="../vendors/parsleyjs/dist/parsley.min.js"></script>
    <!-- Autosize -->
    <script src="../vendors/autosize/dist/autosize.min.js"></script>
    <!-- jQuery autocomplete -->
    <script src="../vendors/devbridge-autocomplete/dist/jquery.autocomplete.min.js"></script>
    <!-- starrr -->
    <script src="../vendors/starrr/dist/starrr.js"></script>
    <!-- Custom Theme Scripts -->
    <script src="../build/js/custom.min.js"></script>

  </body>
</html>
