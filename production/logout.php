<?php

   session_start();
   if(isset($_SESSION['id']))
   {
     session_destroy();
     unset($_SESSION['id']);
     unset($_SESSION['name']);
     // header('location : home.php');
     // echo "log out successful!";
     // echo "<a href='log_in.php'>login</a> again!";
     header('Location: log_in.php');
   }

 ?>
