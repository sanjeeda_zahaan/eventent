<?php
/////////DONE///////////
require_once('../includes/_connection.php');
require_once('../includes/_session.php');

//preventing from entering in the home page without login with correct email and pass
if($_SESSION['id']!=session_id())
{
header('Location :log_in.php');
}

?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>DataTables | Gentelella</title>

    <!-- Bootstrap -->
    <link href="../vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="../vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="../vendors/nprogress/nprogress.css" rel="stylesheet">
    <!-- iCheck -->
    <link href="../vendors/iCheck/skins/flat/green.css" rel="stylesheet">
    <!-- Datatables -->
    <link href="../vendors/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
    <link href="../vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css" rel="stylesheet">
    <link href="../vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css" rel="stylesheet">
    <link href="../vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css" rel="stylesheet">
    <link href="../vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css" rel="stylesheet">

    <!-- Custom Theme Style -->
    <link href="../build/css/custom.min.css" rel="stylesheet">
  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              <a href="index.html" class="site_title"><i class="fa fa-paw"></i> <span>Gentelella Alela!</span></a>
            </div>

            <div class="clearfix"></div>

            <!-- menu profile quick info -->
            <!-- //////////////////// side menu te name \\\\\\\\\\\\\\\\\\ -->
            <div class="profile clearfix">
              <div class="profile_pic">

<!-- ......................... side menu picture of the user........................................ -->
                <?php
                // echo "ID: " . $_SESSION['email'];
                $table = mysqli_query($connection, 'SELECT * FROM register WHERE phone =' . $_SESSION['phone'])or die("Error: " . mysqli_error($connection));
                // $table = mysqli_query($connection, "SELECT * FROM register WHERE email =" . $_SESSION['email']);
                while($row  = mysqli_fetch_array($table)){ ?>


                        <div class="profile_img">

                        <?php  if ($row['profileimg'] == "") {
                        echo "<img src = 'images/default-avatar.jpg' alt = '...' class='img-circle profile_img'>" . "</br>";
                        } else {
                        echo "<img src = 'images/" . $row['profileimg'] . "' alt = '...' class='img-circle profile_img'>" . "</br>";
                        }
                        ?>
                      </div>


              <?php }
              ?>
<!-- ........................ end of side menu picture ................................................ -->


                <!-- <img src="images/img.jpg" alt="..." class="img-circle profile_img"> -->
              </div>
              <div class="profile_info">
                <span>Welcome,</span>
                <h2> <?php echo $_SESSION['name']; ?></h2>
              </div>
            </div>
            <!-- /menu profile quick info -->
            <br />
<!-- ....................................... start of menu options....................................... -->
            <!-- sidebar menu -->
            <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
              <div class="menu_section">
                <!-- <h3></h3> -->
                <ul class="nav side-menu">
                  <!-- ///////////////////////// home \\\\\\\\\\\\\\\\\\\\\ -->
                  <li><a href="profile.php"><i class="fa fa-home"></i> Home <span class="fa fa-chevron"></span></a>

                  </li>
                  <!-- //////////////////////////////////// inbox\\\\\\\\\\\\\\\\\\\\\\\\\ -->
                  <li><a><i class="fa fa-envelope"></i> Inbox <span class="fa fa-chevron"></span></a>

                  </li>
                  <!-- //////////////////////////////////// inbox\\\\\\\\\\\\\\\\\\\\\\\\\ -->
                  <li><a><i class="fa fa-bell"></i> Notifications <span class="fa fa-chevron"></span></a>

                  </li>
                  <!-- ///////////////////////////// create event \\\\\\\\\\\\\\\\\\\\\\\\\ -->
                  <li><a href="create_event.php"><i class="fa fa-edit"></i> Create Event <span class="fa fa-chevron"></span></a>

                  </li>
                  <!-- /////////////////////////////////// Trending event \\\\\\\\\\\\\\\\\\\\\\\\ -->
                  <li><a><i class="fa fa-volume-up"></i> Trending Events <span></span></a>

                  </li>
                  <!-- /////////////////////////////////// search organization \\\\\\\\\\\\\\\\\\\\\\\\ -->
                  <li><a  href="search_user.php"><i class="fa fa-group"></i> Organizations <span></span></a>

                  </li>
                  <!-- /////////////////////////////// my events \\\\\\\\\\\\\\\\\\\\\ -->
                  <li><a><i class="fa fa-desktop"></i> My Events <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="my_event_list.php">Launched</a></li>
                      <li><a href="following_events.php">Attending</a></li>
                      <li><a href="inbox.html">Interested</a></li>
                      <li><a href="calendar.html">Drafts</a></li>
                    </ul>
                  </li>

                  <!-- /////////////////////////////////// privacy and settings \\\\\\\\\\\\\\\\\\\\\\\\ -->
                  <li><a><i class="fa fa-wrench"></i> Privacy & Settings <span class="
                  fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="changePass_old.php">Change Password</a></li>
                    </ul>
                  </li>
                  <!-- ///////////////////////////////////// log out \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\ -->
                <li><a href="logout.php"><i class="fa fa-sign-out"></i> Log Out <span></span></a>
              <!-- //////////////////////////////////////////////////////////////////////////// -->
                </ul>
              </div>
            </div>
            <!-- /sidebar menu -->
<!-- ......................end of side menu .................................... -->


  <!-- ...............nav bar............................           -->
</div>
      </div>
      <!-- top navigation -->
      <div class="top_nav">
        <div class="nav_menu">
          <nav>
            <div class="nav toggle">
              <a id="menu_toggle"><i class="fa fa-bars"></i></a>
            </div>

            <ul class="nav navbar-nav navbar-right">
              <li class="">
                <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                <img src="images/tent2.png" alt="">
                 <?php echo $_SESSION['name']; ?>
                </a>
              </li>
            </ul>
          </nav>
        </div>
      </div>

  <!-- .................................................................... -->
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>Buyer List</h3>
              </div>

              <div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                  <div class="input-group">
                    <input type="text" class="form-control" placeholder="Search for...">
                    <span class="input-group-btn">
                      <button class="btn btn-default" type="button">Go!</button>
                    </span>
                  </div>
                </div>
              </div>
            </div>

            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">

                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <!--
                    <p class="text-muted font-13 m-b-30">
                      DataTables has most features enabled by default, so all you need to do to use it with your own tables is to call the construction function: <code>$().DataTable();</code>
                    </p>
                      -->
                    <table id="datatable" class="table table-striped table-bordered">
                      <thead>
                        <tr>
                          <th>Name</th>
                          <th>Email Address</th>
                          <th>Phone Number</th>
                          <th>Ticket Quantity</th>
<!--                          <th>Code</th>-->
                        </tr>
                      </thead>

                      <tbody>
                      <?php
                      $query= "select register.*, sum(registration.amount) as quantity
                               from event_table
                               JOIN registration ON event_table.event_id = registration.event_id
                               JOIN register ON registration.user_id = register.id
                               GROUP BY register.id ";
                      $result = mysqli_query($connection, $query);
                      $data = mysqli_fetch_all($result, MYSQLI_ASSOC);
                      ?>

                      <?php foreach ($data as $row): ?>
                      <tr>
                          <td><?= $row['name'] ?></p></td>
                          <td><?= $row['email'] ?></p></td>
                          <td><?= $row['phone'] ?></p></td>
                          <td><?= $row['quantity'] ?></p></td>
                        </tr>
                      <?php endforeach; ?>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>


            </div>
          </div>
        </div>
        <!-- /page content -->

        <!-- footer content -->
        <footer>
          <div class="pull-right">
            Gentelella - Bootstrap Admin Template by <a href="https://colorlib.com">Colorlib</a>
          </div>
          <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->
      </div>
    </div>

    <!-- jQuery -->
    <script src="../vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="../vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="../vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="../vendors/nprogress/nprogress.js"></script>
    <!-- iCheck -->
    <script src="../vendors/iCheck/icheck.min.js"></script>
    <!-- Datatables -->
    <script src="../vendors/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="../vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="../vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="../vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
    <script src="../vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
    <script src="../vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="../vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="../vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
    <script src="../vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
    <script src="../vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="../vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
    <script src="../vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>
    <script src="../vendors/jszip/dist/jszip.min.js"></script>
    <script src="../vendors/pdfmake/build/pdfmake.min.js"></script>
    <script src="../vendors/pdfmake/build/vfs_fonts.js"></script>

    <!-- Custom Theme Scripts -->
    <script src="../build/js/custom.min.js"></script>

  </body>
</html>
