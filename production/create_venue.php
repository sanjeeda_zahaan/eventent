<?php require_once('top_navbar.php'); ?>

        <!-- page content -->
        <div class="right_col" role="main">
            <div class="">
                <div class="page-title">
                    <div class="title_left">
                        <h3>Register A New Venue</h3>
                    </div>

                </div>
                <div class="clearfix"></div>

                <!-- Main Form -------------->
                <div class="row">
                    <div class="col-md-12 col-xs-12">
                        <div class="x_panel">
                            <div class="x_title">
                                <h2>Give detail information</h2>
                                <ul class="nav navbar-right panel_toolbox">
                                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                    </li>
                                </ul>
                                <div class="clearfix"></div>
                            </div>
                            <div class="x_content">
                                <?php
                                if(!empty($error)){
                                    echo "<p>$error</p>";
                                }
                                ?>
                                <form class="form-horizontal form-label-left" action="includes/create_venue_reg.php" method="post" enctype="multipart/form-data">

                                    <!-- Venue Name -------------->
                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Venue Name</label>
                                        <div class="col-md-9 col-sm-9 col-xs-12">
                                            <input type="text" class="form-control" name="venue_name" placeholder="Add a name of the venue">
                                        </div>
                                    </div>

                                    <!-- Location -------------->
                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Location</label>
                                        <div class="col-md-9 col-sm-9 col-xs-12">
                                            <input type="text" class="form-control" name="venue_location" placeholder="Add Location">
                                        </div>
                                    </div>

                                    <!-- Capacity -------------->
                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Capacity</label>
                                        <div class="col-md-9 col-sm-9 col-xs-12">
                                            <input type="text" class="form-control" name="venue_capacity" placeholder="">
                                        </div>
                                    </div>

                                    <!--- date and time picker ----->

                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Available From</label>
                                        <div class='col-sm-4'>
                                            Starting Date
                                            <div class="form-group">
                                                <div class='input-group date' id='myDatepicker2'>
                                                    <input type='text' name="venue_available_from" class="form-control" />
                                                    <span class="input-group-addon">
                                                        <span class="glyphicon glyphicon-calendar"></span>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>

                                    </div>

                                    <!--- ending date and time picker ----->
                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Available Till</label>
                                        <div class='col-sm-4'>
                                            Ending Date
                                            <div class="form-group">
                                                <div class='input-group date' id='myDatepicker4'>
                                                    <input type='text' name="venue_available_till" class="form-control" />
                                                    <span class="input-group-addon">
                               <span class="glyphicon glyphicon-calendar"></span>
                            </span>
                                                </div>
                                            </div>
                                        </div>

                                    </div>

                                    <!--- EVENT TYPE --->

                                        <div class="form-group row">
                                            <label class="col-md-3 col-sm-3  control-label">Select Event Type </label>

                                            <div class="col-md-9 col-sm-9 ">
                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" class="flat" id="venue_event_type" name="venue_event_type[]" value="corporate_event" > Corporate Events
                                                    </label>
                                                </div>
                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" class="flat" id="venue_event_type" name="venue_event_type[]" value="birthday_anniversary_event" > Birthday/Anniversary Party
                                                    </label>
                                                </div>
                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" class="flat" id="venue_event_type" name="venue_event_type[]" value="wedding_event"> Wedding Events
                                                    </label>
                                                </div>
                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" class="flat" id="venue_event_type" name="venue_event_type[]" value="fair" > Fair
                                                    </label>
                                                </div>
                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" class="flat" id="venue_event_type" value="indoor_sport_event[]" > Indoor Sport Events
                                                    </label>
                                                </div>
                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" class="flat" id="venue_event_type" value="outdoor_sport_event[]"> Outdoor Sport Events
                                                    </label>
                                                </div>
                                            </div>
                                        </div>

                                    <!--- text area -------->
                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Description</label>
                                        <div class="col-md-9 col-sm-12 col-xs-12">
                                            <div class="x_panel">
                                                <div class="x_title">
                                                    <h2>Description Area<small>Sessions</small></h2>
                                                    <ul class="nav navbar-right panel_toolbox">
                                                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                                        </li>
                                                        <li class="dropdown">
                                                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                                                            <ul class="dropdown-menu" role="menu">
                                                                <li><a href="#">Settings 1</a>
                                                                </li>
                                                                <li><a href="#">Settings 2</a>
                                                                </li>
                                                            </ul>
                                                        </li>
                                                        <li><a class="close-link"><i class="fa fa-close"></i></a>
                                                        </li>
                                                    </ul>
                                                    <div class="clearfix"></div>
                                                </div>
                                                <div class="x_content">
                                                    <div id="alerts"></div>
                                                    <div class="btn-toolbar editor" data-role="editor-toolbar" data-target="#editor-one">
                                                        <div class="btn-group">
                                                            <a class="btn dropdown-toggle" data-toggle="dropdown" title="Font"><i class="fa fa-font"></i><b class="caret"></b></a>
                                                            <ul class="dropdown-menu">
                                                            </ul>
                                                        </div>

                                                        <div class="btn-group">
                                                            <a class="btn dropdown-toggle" data-toggle="dropdown" title="Font Size"><i class="fa fa-text-height"></i>&nbsp;<b class="caret"></b></a>
                                                            <ul class="dropdown-menu">
                                                                <li>
                                                                    <a data-edit="fontSize 5">
                                                                        <p style="font-size:17px">Huge</p>
                                                                    </a>
                                                                </li>
                                                                <li>
                                                                    <a data-edit="fontSize 3">
                                                                        <p style="font-size:14px">Normal</p>
                                                                    </a>
                                                                </li>
                                                                <li>
                                                                    <a data-edit="fontSize 1">
                                                                        <p style="font-size:11px">Small</p>
                                                                    </a>
                                                                </li>
                                                            </ul>
                                                        </div>

                                                        <div class="btn-group">
                                                            <a class="btn" data-edit="bold" title="Bold (Ctrl/Cmd+B)"><i class="fa fa-bold"></i></a>
                                                            <a class="btn" data-edit="italic" title="Italic (Ctrl/Cmd+I)"><i class="fa fa-italic"></i></a>
                                                            <a class="btn" data-edit="strikethrough" title="Strikethrough"><i class="fa fa-strikethrough"></i></a>
                                                            <a class="btn" data-edit="underline" title="Underline (Ctrl/Cmd+U)"><i class="fa fa-underline"></i></a>
                                                        </div>

                                                        <div class="btn-group">
                                                            <a class="btn" data-edit="insertunorderedlist" title="Bullet list"><i class="fa fa-list-ul"></i></a>
                                                            <a class="btn" data-edit="insertorderedlist" title="Number list"><i class="fa fa-list-ol"></i></a>
                                                            <a class="btn" data-edit="outdent" title="Reduce indent (Shift+Tab)"><i class="fa fa-dedent"></i></a>
                                                            <a class="btn" data-edit="indent" title="Indent (Tab)"><i class="fa fa-indent"></i></a>
                                                        </div>

                                                        <div class="btn-group">
                                                            <a class="btn" data-edit="justifyleft" title="Align Left (Ctrl/Cmd+L)"><i class="fa fa-align-left"></i></a>
                                                            <a class="btn" data-edit="justifycenter" title="Center (Ctrl/Cmd+E)"><i class="fa fa-align-center"></i></a>
                                                            <a class="btn" data-edit="justifyright" title="Align Right (Ctrl/Cmd+R)"><i class="fa fa-align-right"></i></a>
                                                            <a class="btn" data-edit="justifyfull" title="Justify (Ctrl/Cmd+J)"><i class="fa fa-align-justify"></i></a>
                                                        </div>

                                                        <div class="btn-group">
                                                            <a class="btn dropdown-toggle" data-toggle="dropdown" title="Hyperlink"><i class="fa fa-link"></i></a>
                                                            <div class="dropdown-menu input-append">
                                                                <input class="span2" placeholder="URL" type="text" data-edit="createLink" />
                                                                <button class="btn" type="button">Add</button>
                                                            </div>
                                                            <a class="btn" data-edit="unlink" title="Remove Hyperlink"><i class="fa fa-cut"></i></a>
                                                        </div>

                                                        <div class="btn-group">
                                                            <a class="btn" title="Insert picture (or just drag & drop)" id="pictureBtn"><i class="fa fa-picture-o"></i></a>
                                                            <input type="file" data-role="magic-overlay" data-target="#pictureBtn" data-edit="insertImage" />
                                                        </div>

                                                        <div class="btn-group">
                                                            <a class="btn" data-edit="undo" title="Undo (Ctrl/Cmd+Z)"><i class="fa fa-undo"></i></a>
                                                            <a class="btn" data-edit="redo" title="Redo (Ctrl/Cmd+Y)"><i class="fa fa-repeat"></i></a>
                                                        </div>
                                                    </div>

                                                    <div id="editor-one" class="editor-wrapper"></div>
                                                    <br />
                                                </div>
                                            </div>
                                        </div>


                                    </div>
                                    <input type="hidden" name="descr" id="descr" placeholder="Add rules and other descriptions"/>
                                    <!------ /text area -------->


                                    <!---------- image --------->
                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Event Image</label>
                                        <div class="col-md-9 col-sm-12 col-xs-12">
                                            <div class="x_panel">
                                                <div class="x_title">
                                                    <h2>Insert Image</h2>

                                                    <input type="file" name="venue_image" accept="image/*" class="form-control">

                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                    <!--- image ---->

                                    <!--- ticket price ---->
                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Price</label>
                                        <div class="col-md-9 col-sm-9 col-xs-12">
                                            <input type="number" name="venue_price" class="form-control" placeholder="">
                                        </div>
                                    </div>


                                    <!--- privacy ----->
                                    <div class="ln_solid"></div>

                                    <div class="form-group">
                                        <div class="col-md-9 col-sm-9 col-xs-12 col-md-offset-3">
                                            <button type="reset" class="btn btn-primary">Reset</button>
                                            <!--                                  <button type="reset" class="btn btn-primary" name="save">Save</button>-->
                                            <button type="submit" class="btn btn-success" name="submitted">Launch Now</button>
                                        </div>
                                    </div>

                                </form>
                                <br />
                            </div>
                        </div>
                    </div>
                </div>
                <!------------  /Form ------------->
            </div>
        </div>
        <!-- /page content -->

        <!-- footer content -->
        <footer>
            <div class="pull-right">
                Gentelella - Bootstrap Admin Template by <a href="https://colorlib.com">Colorlib</a>
            </div>
            <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->
    </div>
</div>

<!-- jQuery -->
<script src="../vendors/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap -->
<script src="../vendors/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- FastClick -->
<script src="../vendors/fastclick/lib/fastclick.js"></script>
<!-- NProgress -->
<script src="../vendors/nprogress/nprogress.js"></script>
<!-- bootstrap-progressbar -->
<script src="../vendors/bootstrap-progressbar/bootstrap-progressbar.min.js"></script>
<!-- iCheck -->
<script src="../vendors/iCheck/icheck.min.js"></script>
<!-- bootstrap-daterangepicker -->
<script src="../vendors/moment/min/moment.min.js"></script>
<script src="../vendors/bootstrap-daterangepicker/daterangepicker.js"></script>
<!-- bootstrap-datetimepicker -->
<script src="../vendors/bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>
<!-- bootstrap-wysiwyg -->
<script src="../vendors/bootstrap-wysiwyg/js/bootstrap-wysiwyg.min.js"></script>
<script src="../vendors/jquery.hotkeys/jquery.hotkeys.js"></script>
<script src="../vendors/google-code-prettify/src/prettify.js"></script>
<!-- jQuery Tags Input -->
<script src="../vendors/jquery.tagsinput/src/jquery.tagsinput.js"></script>
<!-- Switchery -->
<script src="../vendors/switchery/dist/switchery.min.js"></script>
<!-- Select2 -->
<script src="../vendors/select2/dist/js/select2.full.min.js"></script>
<!-- Parsley -->
<script src="../vendors/parsleyjs/dist/parsley.min.js"></script>
<!-- Autosize -->
<script src="../vendors/autosize/dist/autosize.min.js"></script>
<!------------ jQuery autocomplete ------------>
<script src="../vendors/devbridge-autocomplete/dist/jquery.autocomplete.min.js"></script>
<!-- starrr -->
<script src="../vendors/starrr/dist/starrr.js"></script>
<!-- Custom Theme Scripts -->
<script src="../build/js/custom.min.js"></script>

<script>
    $('#myDatepicker2').datetimepicker({
        format: 'YYYY-MM-DD'
    });

    $('#myDatepicker3').datetimepicker({
        format: 'HH:mm'
    });
    $('#myDatepicker4').datetimepicker({
        format: 'YYYY-MM-DD'
    });

    $('#myDatepicker5').datetimepicker({
        format: 'HH:mm'
    });

    $("form").submit(
        function () {
            $("#descr").val($("#editor-one").html());
            return true;
        }
    )
</script>


</body>
</html>
