<?php
/**
 *
 */
class DbConnect
{
    private $host="localhost";
    private $dbname="eventent";
    private $user="root";
    private $pass="";


  public function connect()
  {
    try {
      $conn=new PDO('mysql:host=' . $this->host . '; dbname=' . $this->dbname, $this->user, $this->pass);
      $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
      return $conn;

    } catch (PDOException $e) {
       echo 'Database error: ' . $e->getMessage();
    }

  }
}

 ?>
