<?php

session_start();
//header("Content-Type: text/json");
require 'users.php';
require 'contactUs_save.php';
require 'PHPMailerAutoload.php';
require 'credential.php';


/////////////////////////////// contact us \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

if(isset($_POST['action'])&& $_POST['action']=='sendmail')
{
  $users=validateContactForm();

  $objUser=new ContactUs();

  $objUser->setEmail($users['email']);

  $objUser->setSub($users['sub']);

  $objUser->setMessage($users['message']);

  $objUser->setContact_date(date('Y-m-d H:i:s'));

  if($objUser->save())
  {
    //header("Location: ../INtravel/index.php");
    echo json_encode(["status"=>1,"msg"=>"Message Sent!"]);
    exit;
 }
 else {
   echo json_encode(["status"=>0,"msg"=>"Message sending failed!"]);
   exit;
 }
}

///////////////////////////// image submission \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
if (isset($_POST['imgSubmit'])) {

    $users=validateImage();

}

///////////////////////////////// change pass a old pass check \\\\\\\\\\\\\\\\\\\\
if(isset($_POST['action'])&& $_POST['action']=='UpdatePassword')
{
  $users=validatePassForm();
  $objUser=new Users();
  $objUser->setEmail($_POST['uemail']);

  $objUser->setPassword(md5($users['old_password']));
  $userData= current($objUser->getUserByEmail());

  if (is_array($userData) && count($userData) > 0) {

    if ($userData['password']==$objUser->getPassword()) {

        echo json_encode(["status"=>1,"msg"=>"success!!!"]);
  }else {
  echo json_encode(["status"=>0,"msg"=>"Ivalid password.Please try again."]);
  }

}
}

//////////////////////////////////////// update password "change" r time a \\\\\\\\\\\\\\\\\\\\\\\\\
if(isset($_POST['action'])&& $_POST['action']=='changePass')
{
  $users=validateChangePassForm();
  $objUser=new Users();
  $objUser->setEmail($_POST['email']);
  $userData= current($objUser->getUserByEmail());
  if(is_array($userData) && count($userData) > 0)
  {
              $objUser->setPassword(md5($users['password']));

                      if ($objUser->ChangePass())
                       {
                        echo json_encode(["status"=>1,"msg"=>"Password updated."]);
                        exit;
                      }else {
                        echo json_encode(["status"=>0,"msg"=>"Failed to update password."]);
                        exit;
                      }
  }
  else
  {
    echo json_encode(["status"=>0,"msg"=>"User not found."]);
    exit;
  }

}
//////////////////////////////////// remember me \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
if(isset($_POST['action'])&& $_POST['action']=='checkCookie')
{
   if(isset($_COOKIE['mail'], $_COOKIE['pwd']))
   {
     $data=['email'=>$_COOKIE['mail'],'password'=>base64_decode($_COOKIE['pwd'])];
     //echo ' act3';
     echo json_encode($data);
     //echo json_encode(array("someKey" => "someValue"));
     //echo json_encode([$data]);
     //echo json_encode($data=['email'=>$_COOKIE['mail'],'password'=>base64_decode($_COOKIE['pwd'])]);
   }
}

//////////////////////////////// reset password \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

if(isset($_POST['action'])&& $_POST['action']=='resetPass')
{
  $email = filter_input(INPUT_POST,'remail',FILTER_VALIDATE_EMAIL);
      if(false == $email)
      {
        echo json_encode(["status"=>0,"msg"=>"Enter valid email."]);
          exit;
      }

     $objUser=new Users();
     $objUser->setEmail($email);
     $userData= current($objUser->getUserByEmail()); //getUserByEmail a fetch all thakle current use krte hobe. or only fetch thakle current keyword use krte hobe na
     //$userData= $objUser->getUserByEmail();

  if(is_array($userData) && count($userData)>0)
  {
       $data['id']=$userData['id'];
       $data['token']=sha1($userData['email']);
       $data['expTime']=date('d-m-Y h:i:s', time() + (60*60*2));
       $urlToken=base64_encode(json_encode($data));
       $objUser->setId($userData['id']);
       $objUser->setToken($data['token']);
           if($objUser->updateToken())
           {
                $url='http://' . $_SERVER['SERVER_NAME'] . '/project/production/resetPass_update.php?token=' . $urlToken;
                 $html='<div>You have requested for a password reset for your user account.
                 Please click on the link.:<br>'.$url.'<br>
                 Note: This link is valid for 2 hours.</div>';
                 ///////////////////////sending new token for reset password \\\\\\\\\\\\\\\\\\\\
                           $mail = new PHPMailer;
                           $mail->isSMTP();
                           $mail->Host = 'smtp.gmail.com';
                           $mail->SMTPAuth = true;
                           $mail->Username = EMAIL;
                           $mail->Password = PASS;
                           $mail->SMTPSecure = 'tls';
                           $mail->Port = 587;
                           $mail->setFrom(EMAIL,'Eventent');
                           $mail->addAddress(  $objUser->getEmail());
                           $mail->addReplyTo(EMAIL);
                           $mail->isHTML(true);
                           $mail->Subject = 'Reset your password.';
                           $mail->Body    = $html;
                           if(!$mail->send()) {
                               echo json_encode(["status"=>0,"msg"=>"Message could not be sent."]);
                                  echo json_encode(["status"=>0,"msg"=>'Mailer Error: ' . $mail->ErrorInfo]);
                           } else {
                               echo json_encode(["status"=>1,"msg"=>"Reset password link is sent in your email."]);
                           }
           }
           else
           {
                echo json_encode(["status"=>0,"msg"=>"Some problem has occured.Please try after some time."]);
           }
     }

     else
     {
         echo json_encode(["status"=>0,"msg"=>"User not found!"]);
     }

   }

   ////////////////////////////update password(reset r time a )\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

   if(isset($_POST['action'])&& $_POST['action']=='updatePass')
   {
     $users=validateUpdatePassForm();
     $data=json_decode(base64_decode($users['token']),true);
     $currTime=strtotime(date('d-m-Y h:i:s'));
     $expTime=strtotime($data['expTime']);
     if($currTime > $expTime)
     {
       echo json_encode(["status"=>0,"msg"=>"Token expired! Please try again."]);
       exit;
     }
     $objUser=new Users();
     $objUser->setId($data['id']);
     $userData= $objUser->getUserById();
     if(is_array($userData) && count($userData) > 0)
     {
            if ($data['token']==$userData['token'])
            {
                     $objUser->setPassword(md5($users['password']));

                         if ($objUser->updatePass())
                          {
                           echo json_encode(["status"=>1,"msg"=>"Password updated."]);
                           exit;
                         }else {
                           echo json_encode(["status"=>0,"msg"=>"Failed to update password."]);
                           exit;
                         }
            }
           else
           {
              echo json_encode(["status"=>0,"msg"=>"Token is not valid."]);
              exit;
           }

     }else
     {
       echo json_encode(["status"=>0,"msg"=>"User not found."]);
       exit;
     }

   }

//////////////////////////////// sign up validations actions\\\\\\\\\\\\\\\\\\\\\\\\\\

if(isset($_POST['action'])&& $_POST['action']=='register')
{
  $users=validateRegForm();

  $objUser=new Users();
  $objUser->setName($users['fname']);
  $objUser->setPhone($users['phone']);
  $objUser->setEmail($users['email']);
  $objUser->setPassword(md5($users['password']));
  $objUser->setActivated(0);
  $objUser->setToken(NULL);
  $objUser->setCreatedOn(date('Y-m-d H:i:s'));

  $userData= current($objUser->getUserByEmail());
  if ($userData['email']==$users['email']) {
    echo 'This email is already registered!';
    exit;
  }
  if($objUser->save())
  {
    $lastId=$objUser->conn->lastInsertId();
    $token=sha1($lastId);
    $url='http://' . $_SERVER['SERVER_NAME'] . '/project/production/verify.php?id=' . $lastId . '&token=' .$token;
    $html='<div>You are signed up.Verify your email by clicking on the link:<br>'.$url.'</div>';

////////////////////////////////email verification\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
          // require 'PHPMailerAutoload.php';
          // require 'credential.php';
          $mail = new PHPMailer;
        // $mail->SMTPDebug = 4;                   // to watch the whole process
          $mail->isSMTP();                         // Set mailer to use SMTP
          $mail->Host = 'smtp.gmail.com';          // Specify main and backup SMTP servers
          $mail->SMTPAuth = true;                  // Enable SMTP authentication
          $mail->Username = EMAIL;                 // SMTP username
          $mail->Password = PASS;                  // SMTP password
          $mail->SMTPSecure = 'tls';               // Enable TLS encryption, `ssl` also accepted
          $mail->Port = 587;                       // TCP port to connect to

          $mail->setFrom(EMAIL,'Eventent');
          $mail->addAddress(  $objUser->getEmail());     // Add a recipient
                    // Name is optional
          $mail->addReplyTo(EMAIL);

          //$mail->addAttachment('/var/tmp/file.tar.gz');         // Add attachments
          //$mail->addAttachment('/tmp/image.jpg', 'new.jpg');    // Optional name
          $mail->isHTML(true);                                  // Set email format to HTML

          $mail->Subject = 'Confirm your email.';
          $mail->Body    = $html;
          //$mail->AltBody = $_POST['message'];

          if(!$mail->send()) {
              echo 'Message could not be sent.';
              echo 'Mailer Error: ' . $mail->ErrorInfo;
          } else {
              echo 'Congratulations!Your registration is completed.Now,verify your email.';
          }
    }
  else {
    echo "Sign Up process failed.";
  }

}
///////////////////////////////log in form validation and actions\\\\\\\\\\\\\\\\\\\\\\\\\

if(isset($_POST['action'])&& $_POST['action']=='submit')
{
  $users=validateLoginForm();
  $objUser=new Users();
  $objUser->setEmail($users['mail']);
  $objUser->setPassword(md5($users['pwd']));
  $userData= current($objUser->getUserByEmail());
  $rememberMe=isset($_POST['remember']) ? 1 : 0;

  if (is_array($userData) && count($userData) > 0) {

    if ($userData['password']==$objUser->getPassword()) {

      if ($userData['activated'] == 1 ) {

        if ($rememberMe == 1) {
          setcookie('mail',$objUser->getEmail());
          setcookie('pwd',base64_encode($users['pwd']));
        }
        $_SESSION['id']=session_id(); //for preventing from entering in the home without login
        $_SESSION['name']=$userData['name'];
      //  echo $userData['name'];
        $_SESSION['user_id'] = $userData['id'];
        $_SESSION['phone']=$userData['phone'];
        $_SESSION['email']=$userData['email'];
        $_SESSION['user_id']=$userData['id'];


         echo json_encode(["status"=>1,"msg"=>"Login successfull!"]);

      }else {
          echo json_encode(["status"=>0,"msg"=>"Please verify your email."]);
      }
    }else {
      echo json_encode(["status"=>0,"msg"=>"Ivalid email or password.Please try again."]);
    }
  }else {
  echo json_encode(["status"=>0,"msg"=>"Ivalid email or password.Please try again."]);
  }

}

/////////////////////////////////// Password chnage old pass check \\\\\\\\\\\\\\\\\\\\\\\

function validatePassForm()
{

  $users['old_password']=filter_input(INPUT_POST,'old_password',FILTER_SANITIZE_STRING);
  if(false==$users['old_password'])
  {
    echo json_encode(["status"=>0,"msg"=>"Enter valid password."]);
      exit;
  }
  return $users;
}

//////////////////////////////////// log in validate form \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\


function validateLoginForm()
{
  $users['mail']=filter_input(INPUT_POST,'mail',FILTER_VALIDATE_EMAIL);
  if(false== $users['mail'])
  {
    echo json_encode(["status"=>0,"msg"=>"Enter valid email."]);
      exit;
  }

  $users['pwd']=filter_input(INPUT_POST,'pwd',FILTER_SANITIZE_STRING);
  if(false==$users['pwd'])
  {
    echo json_encode(["status"=>0,"msg"=>"Enter valid password."]);
      exit;
  }
  return $users;
}

////////////////////////////sign up form validation\\\\\\\\\\\\\\\\\\\\\\\\\\\
function validateRegForm()
{
  $users['fname']=filter_input(INPUT_POST,'fname',FILTER_SANITIZE_STRING);
  if(false==  $users['fname']){
    echo "Enter valid name";
    exit;
  }

  $users['phone']=filter_input(INPUT_POST,'phone',FILTER_SANITIZE_NUMBER_INT);
  if(false==$users['phone']){
    echo "Enter valid number";
      exit;
  }

  $users['email']=filter_input(INPUT_POST,'email',FILTER_VALIDATE_EMAIL);
  if(false==  $users['email']){
    echo "Enter valid email";
      exit;
  }

  $users['password']=filter_input(INPUT_POST,'password',FILTER_SANITIZE_STRING);
  if(false==$users['password']){
    echo "Enter valid password,minimum 6 characters.";
      exit;
  }

  $users['cpassword']=filter_input(INPUT_POST,'cpassword',FILTER_SANITIZE_STRING);
  if(false==$users['cpassword']){
    echo "Enter your valid confirmation password.";
      exit;
  }
  if($users['password']!=$users['cpassword']){
    echo "Password is not matched.";
      exit;
  }
  return $users;
}

///////////////////////////////// contact us validation form \\\\\\\\\\\\\\\\\\\\\\\

function validateContactForm()
{
  $users['email']=filter_input(INPUT_POST,'email',FILTER_VALIDATE_EMAIL);
  if(false==  $users['email']){
    echo "Enter valid email";
      exit;
  }


  $users['sub']=filter_input(INPUT_POST,'sub',FILTER_SANITIZE_STRING);
  if(false==  $users['sub']){
    echo "Not a valid subject";
    exit;
  }

  $users['message']=filter_input(INPUT_POST,'message',FILTER_SANITIZE_STRING);
  if(false==  $users['message']){
    echo "Not a valid message body";
    exit;
  }

  return $users;
}


/////////////////////////////////////change password \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

function validateChangePassForm(){


  $users['password']=filter_input(INPUT_POST,'password',FILTER_SANITIZE_STRING);
  if(false==$users['password']){
      echo json_encode(["status"=>0,"msg"=>"Enter a valid password."]);
      exit;
  }

  $users['cpassword']=filter_input(INPUT_POST,'cpassword',FILTER_SANITIZE_STRING);
  if(false==$users['cpassword']){
    echo json_encode(["status"=>0,"msg"=>"Enter a valid confirmation password."]);
      exit;
  }
  if($users['password']!=$users['cpassword']){
    echo json_encode(["status"=>0,"msg"=>"Password is not matched."]);
      exit;
  }

 return $users;

}

///////////////////////////////////// update password \\\\\\\\\\\\\\\\\\\\\\\\\\\

function validateUpdatePassForm(){

  $users['token']=filter_input(INPUT_POST,'token',FILTER_SANITIZE_STRING);
  if(false==$users['token']){
    echo json_encode(["status"=>0,"msg"=>"Not a valid request."]);
      exit;
  }

  $users['password']=filter_input(INPUT_POST,'password',FILTER_SANITIZE_STRING);
  if(false==$users['password']){
      echo json_encode(["status"=>0,"msg"=>"Enter a valid password."]);
      exit;
  }

  $users['cpassword']=filter_input(INPUT_POST,'cpassword',FILTER_SANITIZE_STRING);
  if(false==$users['cpassword']){
    echo json_encode(["status"=>0,"msg"=>"Enter a valid confirmation password."]);
      exit;
  }
  if($users['password']!=$users['cpassword']){
    echo json_encode(["status"=>0,"msg"=>"Password is not matched."]);
      exit;
  }

 return $users;

}
////////////////////////////// image upload \\\\\\\\\\\\\\\\\\\\\\\\\\\\\
function validateImage(){
$target = "images/" . basename($_FILES['image']['name']);
$profileimg = $_FILES['image']['name'];
//$_FILES["image"]["error"];
if (move_uploaded_file($_FILES['image']['tmp_name'], $target)) {
  // echo json_encode(["status"=>1,"msg"=>"Image upload successful"]);

                // echo "<script>alert('success')</script>";
                $objUser=new Users();
                $objUser->setEmail($_POST['email']);
                $userData= current($objUser->getUserByEmail());
                if(is_array($userData) && count($userData) > 0)
                {
                  $objUser->setProfileimg($profileimg);

                          if ($objUser->updateImage())
                           {
                             header("Location: profile.php");
                          //  header('location : profile.php');
                          //  echo "<script>alert('fail')</script>";
                            //echo "Image upload successful";
                            //$msg = "Image upload successful";
                            //echo json_encode(["status"=>1,"msg"=>"Image updated."]);
                            exit;
                          }else {
                               header("Location: changeImage.php");

                            // echo "Failed to update";
                            echo json_encode(["status"=>0,"msg"=>"Failed to update"]);
                            exit;
                          }
                }
}
 else
      {

      //echo "<script>alert('Thare was problem uploading image')</script>";
        header("Location: profile.php");
      // echo json_encode(["status"=>1,"msg"=>"Thare was problem uploading image"]);
       exit;
      }
}
///////////////////////////////////////////\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

 ?>
