
<div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
    <div class="menu_section">
        <h3>General</h3>
        <ul class="nav side-menu">
            <li><a><i class="fa fa-home"></i> Home <span class="fa fa-chevron-down"></span></a>

            </li>
            <li><a href="create_event.php"><i class="fa fa-edit"></i>Create Event<span class="fa fa-chevron-down"></span></a>

            </li>
            <li><a href=""><i class="fa fa-desktop"></i>Event List<span class="fa fa-chevron-down"></span></a>

                 <ul class="nav child_menu">
                  <li><a href="my_event_list.php">My Events</a></li>
                  <li><a href="following_events.php">Followed Events</a></li>
<!--                  <li><a href="">Drafts</a></li>-->
                 </ul>
            </li>
            <li><a href=""><i class="fa fa-desktop"></i>Follow<span class="fa fa-chevron-down"></span></a>

                <ul class="nav child_menu">
                    <li><a href="follower.php">Followers</a></li>
                    <li><a href="following.php.php">Following</a></li>
                </ul>
            </li>

        </ul>
    </div>
            <li><a href="javascript:void(0)"><i class="fa fa-laptop"></i> Landing Page <span class="label label-success pull-right">Coming Soon</span></a></li>
        </ul>
    </div>

</div>
