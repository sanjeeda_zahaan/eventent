<!--Header-->
<?php require_once "../includes/_session.php"; ?>

<header>
    <div class="container-fluid agile-banner_nav">
        <div class="row header-top">
            <div class="col-md-5 top-left p-0">
                <p><i class="fa fa-phone" aria-hidden="true"></i> Call us : +18044261149 </p>
            </div>
            <div class="col-md-7 top-right p-0">
                <p><i class="fa fa-map-marker" aria-hidden="true"></i> eventent.bd@gmail.com
            </div>
        </div>

        <nav class="navbar navbar-expand-lg navbar-light bg-light p-0">

            <h1><a class="navbar-brand" href="index.php">EvenTent</a></h1>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                    aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse justify-content-center" id="navbarSupportedContent">
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item active">
                        <a class="nav-link" href="index.php">Home <span class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="browse_events.php">Browse Events</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="browse_venue.php">Browse Venue</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link pr-lg-0" href="../production/con_us.php">Contact</a>
                    </li>

                    <?php if(!isset($_SESSION['id'])) : ?>
                        <li class="nav-item">
                            <a class="nav-link pr-lg-0" href="../production/log_in.php">Login/Sign up</a>
                        </li>

                    <?php
                        else : ?>
                            <li class="nav-item">
                            <a class="nav-link pr-lg-0" href="../production/profile.php">My Profile</a>
                         </li>
                    <?php endif; ?>


                    <li class="nav-item cr-event-nv">
                        <a class="nav-link pr-lg-0" href="../production/create_event.php">Create Event</a>
                    </li>
                </ul>
            </div>

        </nav>
    </div>
</header>
<!--Header-->
