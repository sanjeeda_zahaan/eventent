<?php require_once "../includes/_connection.php";


?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>EVENTENT</title>

    <!-- Meta tag Keywords -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta charset="utf-8">
    <meta name="keywords" content="In Travel Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template,
	Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design"/>
    <script type="application/x-javascript">
        addEventListener("load", function () {
            setTimeout(hideURLbar, 0);
        }, false);

        function hideURLbar() {
            window.scrollTo(0, 1);
        }
    </script>
    <!--// Meta tag Keywords -->

    <!-- css files -->
    <link rel="stylesheet" href="css/bootstrap.css"> <!-- Bootstrap-Core-CSS -->
    <link rel="stylesheet" href="css/style.css" type="text/css" media="all"/> <!-- Style-CSS -->
    <link rel="stylesheet" href="css/font-awesome.css"> <!-- Font-Awesome-Icons-CSS -->
    <!-- //css files -->

    <!-- web-fonts -->
    <link href="//fonts.googleapis.com/css?family=Raleway:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i&amp;subset=latin-ext"
          rel="stylesheet">
    <!-- //web-fonts -->

</head>
<body>
<?php include_once "cus_nav.php"; ?>

<div class="innerpage-banner">
    <div class="layer1">
    </div>
</div>
<?php
$id = $_GET['id'];
$query = "select * from event_table WHERE event_id= $id";
$result = mysqli_query($connection, $query);
$data = mysqli_fetch_assoc($result);
?>
<!-- welcome -->
<section class="welcome py-5">
    <div class="container py-md-3">
        <h3 class="heading text-center mb-md-5 mb-4"><?= $data['title'] ?></h3>
        <div class="row welcome-grids">
            <div class="col-lg-6">
                <!--	<h4 class="mb-3"> //  $data['title'] </h4> -->
                <h3>Location: </span><?= $data['ev_location'] ?></h3>
                <h4>Starting
                    From: </span><?= date("d, F h:i a", strtotime($data['sdate'] . ' ' . $data['stime'])) ?></h4>
                <h4>Ending At: </span><?= date("d, F h:i a", strtotime($data['edate'] . ' ' . $data['etime'])) ?></h4>
                <?php
                //$user_id = $_SESSION['user_id'];
                $query = "select * from register WHERE id= $data[user_id]";
                $result = mysqli_query($connection, $query);
                $user_data = mysqli_fetch_assoc($result);
                ?>
                <h4>Organized By: <?=$user_data['name'] ?></h4>
                <p class="my-4"><?= $data['description'] ?></p>


                <h6 style="color: #999; ">Sponsored By: <?=$data['sponsored_by'] ?></h6>
<!--                <h6 style="color: #999 ;">Remaining Tickets: --><?//=$data['remaining'] ?><!-- </h6>-->
                <h6 style="color: #999; ">Price: <?=$data['price'] ?></h6>


                <?php if(!isset($_SESSION['id'])): ?>

                    <a href="../production/log_in.php">Register</a>

                <?php elseif (isset($_SESSION['id']) && $_SESSION['user_id'] == $user_data['id']) : ?>

                    <a href="../production/edit_event.php?id=<?=$id;?>">Edit</a>
                    <a href="../production/buyer_list.php">Registered By</a>

                <?php else : ?>
                    <button type="button" style="margin-top: 10px;" class="btn btn-info btn-lg"  data-toggle="modal" data-target="#myModal">Register
                </button>

                <?php endif; ?>



                <!-- Modal -->
                <div id="myModal" class="modal fade" role="dialog">
                    <div class="modal-dialog">

                        <!-- Modal content-->
                        <div class="modal-content">
                            <form action="../production/buy_tickets.php" method="post">
                                <div class="modal-header">
                                    <h4 class="modal-title">Ticket Quantity</h4>
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                </div>
                                <div class="modal-body">
                                    <input type="hidden" name="id" value="<?= $id ?>" placeholder="1">
                                    <input type="number" name="quantity">

                                </div>
                                <div class="modal-footer">
                                    <button type="submit" class="btn btn-default">Save

                                    </button>
                                </div>
                            </form>
                        </div>

                    </div>
                </div>
            </div>
            <div class="col-lg-6 mt-lg-0 mt-5 welcome-grid3">
                <div class="position">
                    <img src="<?= (!empty($data['cover_image'])) ? "../upload/" . $data['cover_image'] :
                        "images/price.jpg" ?>" alt="" class="img-fluid"/>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- //welcome -->


<!-- footer -->
<footer class="py-5">
    <div class="container py-md-3">
        <div class="row footer-grids pb-md-5 pb-3">
            <div class="col-md-3 col-sm-6 col-6">
                <a href="#"> <i class="fa fa-phone"></i>Call Us</a>
            </div>
            <div class="col-md-3 col-sm-6 col-6">
                <a href="#"> <i class="fa fa-envelope"></i>Send Message</a>
            </div>
            <div class="col-md-3 col-sm-6 col-6 mt-md-0 mt-2">
                <a href="#"> <i class="fa fa-skype"></i>Skype Call</a>
            </div>
            <div class="col-md-3 col-sm-6 col-6 mt-md-0 mt-2">
                <a href="#"> <i class="fa fa-comment"></i>Online Chat</a>
            </div>
        </div>

        <div class="subscribe-grid text-center">
            <p class="para three mt-4">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus at placerat
                ante. Praesent nulla nunc, pretium dapibus efficitur in, auctor eget elit. Lorem ipsum dolor sit
                amet </p>
            <h5>Subscribe for our latest updates</h5>
            <p>Get
                <span>10%</span> off on booking</p>
            <form action="#" method="post">
                <input class="form-control" type="email" placeholder="Subscribe" name="Subscribe" required="">
                <button class="btn1">
                    <i class="fa fa-paper-plane"></i>
                </button>
            </form>
        </div>
    </div>
</footer>
<!-- //footer -->

<!-- copyright -->
<section class="copyright py-4 text-center">
    <div class="container">
        <p>© 2018 In Travel. All Rights Reserved | Design by <a href="http://w3layouts.com/" target="=_blank">
                W3layouts </a></p>
    </div>
</section>
<!-- //copyright -->

<!-- js-scripts -->

<!-- js -->
<script type="text/javascript" src="js/jquery-2.2.3.min.js"></script>
<script type="text/javascript" src="js/bootstrap.js"></script> <!-- Necessary-JavaScript-File-For-Bootstrap -->
<!-- //js -->

<!-- start-smoth-scrolling -->
<script src="js/SmoothScroll.min.js"></script>
<script type="text/javascript" src="js/move-top.js"></script>
<script type="text/javascript" src="js/easing.js"></script>
<script type="text/javascript">
    jQuery(document).ready(function ($) {
        $(".scroll").click(function (event) {
            event.preventDefault();
            $('html,body').animate({scrollTop: $(this.hash).offset().top}, 1000);
        });
    });
</script>
<!-- here stars scrolling icon -->
<script type="text/javascript">
    $(document).ready(function () {
        /*
         var defaults = {
         containerID: 'toTop', // fading element id
         containerHoverID: 'toTopHover', // fading element hover id
         scrollSpeed: 1200,
         easingType: 'linear'
         };
         */

        $().UItoTop({easingType: 'easeOutQuart'});

    });
</script>
<!-- //here ends scrolling icon -->
<!-- start-smoth-scrolling -->

<!-- //js-scripts -->

</body>
</html>
