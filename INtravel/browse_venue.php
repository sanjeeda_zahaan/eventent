<!DOCTYPE html>
<html lang="en">
<head>
    <title>EVENTENT</title>

    <!-- Meta tag Keywords -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta charset="utf-8">
    <meta name="keywords" content="In Travel Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template,
	Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
    <script type="application/x-javascript">
        addEventListener("load", function () {
            setTimeout(hideURLbar, 0);
        }, false);

        function hideURLbar() {
            window.scrollTo(0, 1);
        }
    </script>
    <!--// Meta tag Keywords -->

    <!-- css files -->
    <link rel="stylesheet" href="css/bootstrap.css"> <!-- Bootstrap-Core-CSS -->
    <link rel="stylesheet" href="css/style.css" type="text/css" media="all" /> <!-- Style-CSS -->
    <link rel="stylesheet" href="css/font-awesome.css"> <!-- Font-Awesome-Icons-CSS -->
    <!-- //css files -->

    <!-- web-fonts -->
    <link href="//fonts.googleapis.com/css?family=Raleway:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i&amp;subset=latin-ext" rel="stylesheet">
    <!-- //web-fonts -->

</head>

<body>
<?php
include_once "cus_nav.php";
?>

<div class="innerpage-banner">
    <div class="layer1">
    </div>
</div>


<!-- search event -->
<section class="booking py-5" id="booking">
    <h3 class="text-center mb-4">Search Venue</h3>
    <div class="container">
        <div class="book-form">
            <form action="" method="get">
                <div class="row">
                    <div class="col-md-3 col-sm-6 col-6 px-2 form-time-w3layouts editContent">
                        <label class="editContent"><span class="fa fa-user" aria-hidden="true"></span> Event
                            Location</label>
                        <input type="text" name="title" placeholder="Event Title">
                    </div>
                    <div class="col-md-3 col-sm-6 col-6 px-2 form-date-w3-agileits editContent">
                        <label class="editContent"><span class="fa fa-map-marker" aria-hidden="true"></span>
                            Capacity</label>
                        <input type="text" name="location" placeholder="Event Location">

                    </div>
                    <div class="col-md-2 px-2 col-sm-4 col-6 form-date-w3-agileits editContent">
                        <label class="editContent"><span class="fa fa-map-marker" aria-hidden="true"></span> Event Type</label>
                        <select class="form-control" name="date">
                            <option value=""></option>
                            <option value="1">Corporate Event</option>
                            <option value="7">Wedding Event</option>
                            <option value="30">Party</option>
                            <option value="30">Fair</option>
                        </select>

                    </div>

                    <div class="col-md-2 px-2 col-sm-4 col-6 form-left-agileits-w3layouts editContent">

                        <label class="editContent"><span class="fa fa-bus" aria-hidden="true"></span>Event date</label>
                        <select class="form-control" name="date">
                            <option value=""></option>
                            <option value="1">Tomorrow</option>
                            <option value="7">With In Next Week</option>
                            <option value="30">With In Next Month</option>
                        </select>

                    </div>
                    <div class="col-md-2 px-2 col-sm-4 col-6 form-left-agileits-submit editContent">
                        <input type="submit" name="src" value="Search">
                    </div>
                </div>
            </form>
        </div>
    </div>
</section>
<!-- //search event-->

<?php if(isset($_GET['src'])): ?>
    <!-- search result of events -->
    <section class="best-offers py-5">
        <div class="container py-3">
            <h3 class="heading text-center mb-sm-5 mb-3">Search Result </h3>
            <div class="row offer-grids pt-5">
                <?php
                $title = mysqli_escape_string($conn, trim($_GET['title']));
                $location = mysqli_escape_string($conn, trim($_GET['location']));
                $date = mysqli_escape_string($conn, trim($_GET['date']));
                $type = mysqli_escape_string($conn, trim($_GET['type']));
                $current_date = date("Y-m-d");
                $search_date= date("Y-m-d", strtotime("+$date day"));
                $where = " where 1 ";
                if (!empty($title)) $where .= " AND title like '%$title%' ";
                if (!empty($location)) $where .= " AND ev_location like '%$location%' ";
                if (!empty($type)) $where .= " AND ev_type like '%$type%' ";
                if (!empty($date)) $where .= " AND (sdate > '$current_date' AND sdate <= '$search_date') ";

                $query = "select * from event_table $where ORDER BY event_id DESC limit 8";
                //            echo "$query";
                //            exit($query);
                $result = mysqli_query($conn, $query);
                $data = mysqli_fetch_all($result, MYSQLI_ASSOC);
                ?>
                <?php foreach ($data as $row): ?>
                    <div class="col-lg-3 col-sm-6 offer-grid" style="margin-bottom: 70px;">
                        <img style="height: 200px;"
                             src="<?= (!empty($row['cover_image'])) ? "../upload/" . $row['cover_image'] :
                                 "images/price.jpg" ?>"
                             alt="" class="img-fluid"/>
                        <h4 class="mt-3"><?= $row['title'] ?></h4>
                        <p class="mt-2"><span class="offer_span">Location: </span><?= $row['ev_location'] ?></p>
                        <p class="mt-2"><span>Starting From: </span><?= date("d, F h:i a", strtotime($row['sdate'] . ' ' . $row['stime'])) ?></p>
                        <a class="welcome welcome-grids" href="event_details.php?id=<?=$row['event_id']?>"> Read More</a>
                        <div class="offer">
                            <h3><?= $row['ticket_type'] ?></h3>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>
        </div>

        <div class="mx-auto mt-lg-4 mt-5 text-center agile_inner_info">
            <a href="browse_events.php">Explore all events</a>
        </div>
    </section>
    <!-- //search result of events -->
<?php endif; ?>

<!-- Recent Venues -->
<section class="best-offers py-5">
    <div class="container py-3">
        <h3 class="heading text-center mb-sm-5 mb-3"> Most Recent Events </h3>
        <div class="row offer-grids pt-5">
            <?php
            require_once "../production/includes/server.php";
            $query = "select * from event_table ORDER BY event_id DESC";
            $result = mysqli_query($conn, $query);
            $data = mysqli_fetch_all($result, MYSQLI_ASSOC);
            ?>
            <?php foreach ($data as $row): ?>
                <div class="col-lg-3 col-sm-6 offer-grid" style="margin-bottom: 70px;">
                    <img style="height: 200px;"
                         src="<?= (!empty($row['cover_image'])) ? "../upload/" . $row['cover_image'] :
                             "images/price.jpg" ?>"
                         alt="" class="img-fluid"/>
                    <h4 class="mt-3"><?= $row['title'] ?></h4>
                    <p class="mt-2"><span>Location: </span><?= $row['ev_location'] ?></p>
                    <p class="mt-2"><span>Starting From: </span><?= date("d, F h:i a", strtotime($row['sdate'] . ' ' . $row['stime'])) ?></p>
                    <p class="mt-2"><?= $row['ev_location'] ?></p>
                    <p class="mt-2"><?= $row['ev_type'] ?></p>
                    <a href="event_details.php?id=<?= $row['event_id']?>" class="sub"> Read More</a>

                    <div class="offer">
                        <h3><?= $row['ticket_type'] ?></h3>
                    </div>
                </div>
            <?php endforeach; ?>

        </div>
    </div>
</section>


<!-- testimonials -->
<div class="testimonials pt-lg-5 pb-lg-0 pb-4">
    <div class="container">
        <h3 class="heading text-center mb-md-5 mb-3">Happy Clients</h3>
        <div class="w3_testimonials_grids">
            <div id="slideshow" class="disabled">
                <button class="previous"><b>« Previous</b></button>
                <button class="next"><b>Next »</b></button>
                <div class="strip">
                    <div class="row slide sticky">
                        <div class="col-lg-6 agileinfo_team_grid">
                            <div class="agileinfo_team_grid1">
                                <div class="agileinfo_team_grid1_text">
                                    <div class="agileinfo_team_grid1_pos">
                                        <img src="images/b1.jpg" alt=" " class="img-responsive" />
                                    </div>
                                    <h4>Mark Henry</h4>
                                    <h5>Client 1</h5>
                                    <div class="clearfix"></div>
                                    <p>Sed eu sollicitudin ex. Donec elit malesuada maur ac lectus molestie tristique amet.</p>
                                </div>

                            </div>
                        </div>
                        <div class="col-lg-6 agileinfo_team_grid">
                            <div class="agileinfo_team_grid1">
                                <div class="agileinfo_team_grid1_text">
                                    <div class="agileinfo_team_grid1_pos">
                                        <img src="images/b2.jpg" alt=" " class="img-responsive" />
                                    </div>
                                    <h4>John Mark</h4>
                                    <h5>Client 2</h5>
                                    <div class="clearfix"></div>
                                    <p>Sed eu sollicitudin ex. Donec elit malesuada maur ac lectus molestie tristique amet.</p>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="row slide">
                        <div class="col-lg-6 agileinfo_team_grid">
                            <div class="agileinfo_team_grid1">
                                <div class="agileinfo_team_grid1_text">
                                    <div class="agileinfo_team_grid1_pos">
                                        <img src="images/b1.jpg" alt=" " class="img-responsive" />
                                    </div>
                                    <h4>Thomus Lii</h4>
                                    <h5>Client 3</h5>
                                    <div class="clearfix"></div>
                                    <p>Sed eu sollicitudin ex. Donec elit malesuada maur ac lectus molestie tristique amet.</p>
                                </div>

                            </div>
                        </div>
                        <div class="col-lg-6 agileinfo_team_grid">
                            <div class="agileinfo_team_grid1">
                                <div class="agileinfo_team_grid1_text">
                                    <div class="agileinfo_team_grid1_pos">
                                        <img src="images/b4.jpg" alt=" " class="img-responsive" />
                                    </div>
                                    <h4>Joseph Carl</h4>
                                    <h5>Client 4</h5>
                                    <div class="clearfix"></div>
                                    <p>Sed eu sollicitudin ex. Donec elit malesuada maur ac lectus molestie tristique amet.</p>
                                </div>

                            </div>
                        </div>
                    </div>

                </div>
            </div>

        </div>
    </div>
</div>
<!-- //testimonials -->


<!-- footer -->
<footer class="py-5">
    <div class="container py-md-3">
        <div class="row footer-grids pb-md-5 pb-3">
            <div class="col-md-3 col-sm-6 col-6">
                <a href="#"> <i class="fa fa-phone"></i>Call Us</a>
            </div>
            <div class="col-md-3 col-sm-6 col-6">
                <a href="#"> <i class="fa fa-envelope"></i>Send Message</a>
            </div>
            <div class="col-md-3 col-sm-6 col-6 mt-md-0 mt-2">
                <a href="#"> <i class="fa fa-skype"></i>Skype Call</a>
            </div>
            <div class="col-md-3 col-sm-6 col-6 mt-md-0 mt-2">
                <a href="#"> <i class="fa fa-comment"></i>Online Chat</a>
            </div>
        </div>

        <div class="subscribe-grid text-center">
            <p class="para three mt-4">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus at placerat ante. Praesent nulla nunc, pretium dapibus efficitur in, auctor eget elit. Lorem ipsum dolor sit amet </p>
            <h5>Subscribe for our latest updates</h5>
            <p>Get
                <span>10%</span> off on booking</p>
            <form action="#" method="post">
                <input class="form-control" type="email" placeholder="Subscribe" name="Subscribe" required="">
                <button class="btn1">
                    <i class="fa fa-paper-plane"></i>
                </button>
            </form>
        </div>
    </div>
</footer>
<!-- //footer -->

<!-- copyright -->
<section class="copyright py-4 text-center">
    <div class="container">
        <p>© 2018 In Travel. All Rights Reserved | Design by <a href="http://w3layouts.com/" target="=_blank"> W3layouts </a></p>
    </div>
</section>
<!-- //copyright -->

<!-- js-scripts -->

<!-- js -->
<script type="text/javascript" src="js/jquery-2.2.3.min.js"></script>
<script type="text/javascript" src="js/bootstrap.js"></script> <!-- Necessary-JavaScript-File-For-Bootstrap -->
<!-- //js -->


<!-- clients-slider-script -->
<script src="js/slideshow.min.js"></script>
<script src="js/launcher.js"></script>
<!-- //clients-slider-script -->

<!-- start-smoth-scrolling -->
<script src="js/SmoothScroll.min.js"></script>
<script type="text/javascript" src="js/move-top.js"></script>
<script type="text/javascript" src="js/easing.js"></script>
<script type="text/javascript">
    jQuery(document).ready(function($) {
        $(".scroll").click(function(event){
            event.preventDefault();
            $('html,body').animate({scrollTop:$(this.hash).offset().top},1000);
        });
    });
</script>
<!-- here stars scrolling icon -->
<script type="text/javascript">
    $(document).ready(function() {
        /*
            var defaults = {
            containerID: 'toTop', // fading element id
            containerHoverID: 'toTopHover', // fading element hover id
            scrollSpeed: 1200,
            easingType: 'linear'
            };
        */

        $().UItoTop({ easingType: 'easeOutQuart' });

    });
</script>
<!-- //here ends scrolling icon -->
<!-- start-smoth-scrolling -->

<!-- //js-scripts -->

</body>
</html>
